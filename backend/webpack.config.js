// vim: ts=2: sw=2:
module.exports = {
  context: __dirname,
  entry: ['app'],
  output: {
    filename: 'app.js',
    path: __dirname + '/build/',
    libraryTarget: 'commonjs'
  },

  externals: [
    function (context, request, cb) {
      var isExternal = /^[a-z]/.test(request);
      cb(null, isExternal);
    }
  ],
  target: 'node'
}
