// vim: ts=2: sw=2:
var path = require('path');
var db = require(path.resolve('src/models'));

describe('Test suite for Product model', function () {
  describe('Succesfull', function () {
    it('findOne id=100', function (done) {
      db.Product.model.findOne({where: {id: 100}})
        .then(function() {
          expect(true).toBe(true);
          done();
        }, function () {
          expect(false).toBe(true);
          done();
        });
    });

    it('request type', function (done) {
      db.Product.model.findOne({where: {id: 100}})
        .then(function (product) {
          product.getType().then(function (type) {
            expect(type.name).toBe('Кольца');
            done();
          }, function () {
            expect(false).toBe(true);
            done();
          });
        }, function () {
          expect(false).toBe(true);
          done();
        });
    });

    it('getMany', function (done) {
      db.Product.getMany([1, 2, 3, 4,])
        .then(function (products) {
          expect(true).toBe(true);
          done();
        })
        .fail(function (error) {
          expect(false).toBe(true, error);
          done();
        });
    });
  });

  describe('Failure', function () {
  });
});
