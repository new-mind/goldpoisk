// vim: ts=2: sw=2:
var models = require('../db')
var TEMPLATES_BY_MODEL = {
  'Product': [{
      name: 'По алфавиту',
      value: 'name'
    }, {
      name: 'Сначала дорогие',
      value: 'tprice'
    }, {
      name: 'Сначала дешёвые',
      value: 'price' 
  }]
}

var VALUES_BY_MODEL = {
  'Product': {
    name: [['name', 'ASC']],
    price: [[models.Item.model, 'cost', 'ASC']],
    tprice: [[models.Item.model, 'cost', 'DESC']]
  }
}

var SQL_BY_MODEL = {
  'Product': {
    name: 'ORDER BY p.name ASC ',
    price: 'ORDER BY min ASC ',
    tprice: 'ORDER BY max DESC '
  }
}

function Sort (modelName, key) {
    this.modelName = modelName;
    this.key = key
    this._values = VALUES_BY_MODEL[this.modelName];
}

Sort.prototype = {
    getSQL: function () {
      return SQL_BY_MODEL[this.modelName][this.key] || '';
    },

    getValues: function () {
      return this._values[this.key]
    },

    toJSON: function () {
      return TEMPLATES_BY_MODEL[this.modelName]
    }
}

module.exports = Sort
