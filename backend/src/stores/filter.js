// vim: ts=2: sw=2:
var Q = require('q');
var db = require('../db');
var _ = require('lodash');

/**
 * @param {String} moduleName
 * @param {Object} opts
 *   @key {Object} states
 *     @key {Boolean} hidden
 *     @key {String} shop, 'opened' | 'closed'
 *     @key {String} gem, 'opened' | 'closed'
 *     @key {String} material, 'opened' | 'closed'
 *     @key {String} cost, 'opened' | 'closed'
 *   @key {Number[]} material 
 *   @key {Number[]} [gem] 
 *   @key {Number[]} [shop]
 *   @key {Number} [maxCost] 
 *   @key {Number} [minCost]
 */
function Filter (moduleName, opts) {
  this.moduleName = moduleName;
  this._json = null;

  var maxCost = opts.maxCost ? parseInt(opts.maxCost, 10) : null;
  if (maxCost < 0)
    maxCost = null;

  var minCost = opts.minCost ? parseInt(opts.minCost, 10) : null;
  if (minCost < 0)
    minCost = null;

  this._values = {
    material: _.map(opts.material, function (m) {return parseInt(m, 10);}),
    gem: _.map(opts.gem, function (g) {return parseInt(g, 10);}),
    shop: _.map(opts.shop, function (s) {return parseInt(s, 10);}),
    maxCost: maxCost,
    minCost: minCost
  }

  this._states = {
    hidden: !!opts.states.hidden,
    shop: opts.states.shop || 'opened',
    material: opts.states.material || 'opened',
    gem: opts.states.gem || 'opened',
    cost: opts.states.cost || 'opened',
  }
}

Filter.prototype = {
  getValues: function getValues () {
    return this._values;
  },

  /**
   * Return filter SQL for Product
   */
  getSQL: function getSQL () {
    if (this.moduleName != 'Product')
      throw new Error('Only Product model is supported')

    var sequelize = db.sequelize;

    var output = [];
    _.forIn(this._values, function (value, key) {
      if (!value)
        return;

      if (~['shop', 'material', 'gem'].indexOf(key) && !value.length)
        return;

      var sql = ''
      switch (key) {
        case 'material':
          sql = [
            'INNER JOIN',
              '(SELECT product_id FROM product_feature',
               'INNER JOIN feature',
               'ON product_feature.feature_id = feature.id',
               'INNER JOIN',
                 '(SELECT * from feature.material',
                  'WHERE feature.material.id IN (:materials)) AS fm',
               'ON feature.id = fm.feature_id)',
            'AS fm ON p.id = fm.product_id '
          ].join(' ');
          break;
        case 'shop':
          sql = [
            'INNER JOIN',
              '(SELECT product_id FROM item',
               'INNER JOIN',
                '(SELECT shop.id FROM shop',
                 'WHERE shop.id IN (:shops)) AS fs',
               'ON fs.id = item.shop_id)',
            'AS fi ON p.id = fi.product_id '
          ].join(' ');
          break;
        case 'gem':
          sql = [
            'INNER JOIN',
              '(SELECT product_id FROM product_feature',
               'INNER JOIN feature',
               'ON product_feature.feature_id = feature.id',
               'INNER JOIN',
                 '(SELECT * from feature.gem',
                  'WHERE feature.gem.id IN (:gems)) AS fg',
               'ON feature.id = fg.feature_id)',
            'AS fg ON p.id = fg.product_id '
          ].join(' ');
          break;
        case 'maxCost':
          sql = [
            'INNER JOIN',
              '(SELECT product_id, cost FROM item',
               'WHERE cost <= :maxCost) AS max_fitem',
            'ON max_fitem.product_id = p.id '
          ].join(' ');
          break;
        case 'minCost':
          sql = [
            'INNER JOIN',
              '(SELECT product_id, cost FROM item',
               'WHERE cost >= :minCost) AS min_fitem',
            'ON min_fitem.product_id = p.id '
          ].join(' ');
          break;
        default:
          break;
      }

      output.push(sql);
    })

    return output.join('');
  },

  load: function load (category) {
    var self = this;

    return Q.Promise(function (resolve, reject) {
      Q.all([
        db.Product.getRangeCostForCategory(category),
        db.Material.getForCategory(category),
        db.Gem.getForCategory(category),
        db.Shop.getForCategory(category),
      ])
        .spread(function ([minCost, maxCost], materials, gems, shops) {
          self._json = [];
          if (minCost || maxCost) {
            self._json.push({
              title: 'Цена',
              type: 'price',
              state: self._states.cost,
              value: {
                min: minCost,
                max: maxCost
              }
            });
          }

          if (materials.length) {
            self._json.push({
              title: 'Материал',
              type: 'material',
              state: self._states.material,
              value: materials
            });
          }

          if (gems.length) {
            self._json.push({
              title: 'Камни',
              state: self._states.gem,
              type: 'gem',
              value: gems
            });
          }

          if (shops.length) {
            self._json.push({
              title: 'Магазины',
              state: self._states.shop,
              type: 'shop',
              value: shops
            })
          }
          resolve();
        })
        .fail(function (e) {
          reject(e);
        });
    });
  },

  toJSON: function toJSON () {
    if (!this._json)
      throw new Error('You should use `load` before `toJSON`');

    return {
      hidden: this._states.hidden,
      params: this._json
    };
  }
}

module.exports = Filter;
