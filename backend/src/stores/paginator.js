// vim: ts=2: sw=2:
var SortStore = require('./sort');
var FilterStore = require('./filter');
/**
 * Create new Paginator(..) from nodejs req
 *
 * @param {String} modelName
 * @param {Object} opts, see `Paginator` doc
 * @return {Object} paginator
 */
function createFromRequest (modelName, req) {
  var query = req.query;
  var cookies = req.cookies;

  var opts = {
    currentPage: query.page,
    sort: query.sort,
    filter: {
      'states': {
        'shop': cookies['shop_state'],
        'gem': cookies['gem_state'],
        'material': cookies['material_state'],
        'cost': cookies['price_state'],
        'hidden': cookies['filter_hidden'] === 'true'
      },
      'shop': query.shop ? query.shop.split('.') : null,
      'material': query.material ? query.material.split('.') : null,
      'gem': query.gem ? query.gem.split('.') : null,
      'maxCost': query.max || null,
      'minCost': query.min || null
    }
  }
  return new Paginator(modelName, opts);
}

/**
 * Paginator object for easely store information
 * for filtering/sorting/paginate object
 *
 * @param {String} modelName
 * @param {Object} opts
 *  @key {Number} [opts.currentPage]
 *  @key {Number} [opts.totalPage]
 *  @key {String} [opts.sort], key of sort fields
 *  @key {String} [opts.filter], key of filter fields
 */
function Paginator (modelName, opts) {
  this.modelName = modelName;
  this.currentPage = opts.currentPage || 1;
  if (this.currentPage < 1)
    this.currentPage = 1;

  this.totalPages = opts.totalPages || 1;
  if (this.totalPages < 1)
    this.totalPages = 1;

  this.sort = new SortStore(modelName, opts.sort)
  this.filter = new FilterStore(modelName, opts.filter)
  this.itemsOnPage = 30;
}

Paginator.prototype = {
  update: function (count) {
    var totalPages = Math.ceil(count / this.itemsOnPage);
    if (this.totalPages != totalPages)
      this.totalPages = totalPages || 1;
  },

  toJSON: function (category, path) {
    var url = '/rest/' + category

    return {
      totalPages: this.totalPages,
      currentPage: this.currentPage,
      url: path,
      config: {
        HTTP: {
          list: url 
        }
      }
    };
  }

}

module.exports.Paginator = Paginator;
module.exports.createFromRequest = createFromRequest;
