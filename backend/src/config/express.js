// vim: ts=2: sw=2:
var express = require('express');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');
var engine = require('express-bemhtml-priv.js');
var path = require('path');

var initControllers = require('../controllers');

var router = express.Router();

module.exports = function(app, config) {
  var env = process.env.NODE_ENV || 'development';
  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env == 'development';

  engine.init(app, {
    bemhtml: path.join(config.root, '../public/js/index.bemhtml.js'),
    priv: path.join(config.root, '../public/js/index.priv.js')
  });

  //app.use(favicon(config.root + '/public/img/favicon.ico'));
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(cookieParser());
  app.use(compress());
  app.use(express.static(config.root + '/public'));
  app.use('/media', express.static(config.mediaRoot));
  app.use(methodOverride());

  initControllers(router, config);
  app.use(router);

  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  //TODO: format
  if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
      var status = err.status || 500;
      res.status(status).send(err);
    });
  } else {
    app.use(function (err, req, res, next) {
      var status = err.status || 500;
      var context = {
        code: status,
        description: err.message,
        products: []
      }

      engine.render('pages["error"]', [context, {}])
        .then(function (html) {
          res.status(status).send(html);
        }, function (e) {
          res.status(status).send(e.message);
        });
    });
  }

};
