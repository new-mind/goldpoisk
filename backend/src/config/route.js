// vim: ts=2: sw=2:
var sprintf = require('sprintf');
var _ = require('lodash');
var logger = require('winston');

var db = require('../db');

var COMMON_API = {
  GET: {
    '/$': 'Home',
    '/best$': 'Best'
  }
}


var REST_API = {
  GET: {
    '/rest/products': 'Products'
  },
  POST: {},
  PUT: {},
  DELETE: {}
}

// Refactoring
var categories = db.Type.getCategories();
_.forEach(categories, function (category) {
  var commonUrl = sprintf('/%s$', category);
  var restUrl = sprintf('/rest/%s$', category);

  COMMON_API['GET'][commonUrl] = 'Category';
  REST_API['GET'][restUrl] = 'Category';

  var commonProductUrl = sprintf('/%s/:slug$', category);
  var restProductUrl = sprintf('/rest/%s/:slug$', category);

  COMMON_API['GET'][commonProductUrl] = 'Product';
  REST_API['GET'][restProductUrl] = 'Product';
});

// logging
logRoutes(COMMON_API);
logRoutes(REST_API);
function logRoutes(api) {
  _.forIn(api, function (d, method) {
    logger.info(sprintf('%s:', method));
    _.forIn(d, function (ctrl, route) {
      logger.info('\t', route, '\t', ctrl);
    })
  })
}

module.exports = {
  COMMON_API: COMMON_API,
  REST_API: REST_API
};
