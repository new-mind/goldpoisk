// vim: ts=2: sw=2:
var path = require('path');
var rootPath = path.normalize(__dirname + '/../../');
var env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    mediaDomain: 'http://goldpoisk.ru',
    root: rootPath,
    mediaRoot: path.join(rootPath, '../frontend/media/'),
    app: {
      name: 'backend'
    },
    port: 3000,
    db: 'postgres://dev_goldpoisk:dev12345@goldpoisk.cy58blwqhzwr.eu-central-1.rds.amazonaws.com/production'
  },

  test: {
    root: rootPath,
    mediaRoot: path.join(rootPath, '../frontend/media/'),
    app: {
      name: 'backend'
    },
    port: 3000,
    db: 'postgres://localhost/backend-test'
  },

  production: {
    root: rootPath,
    mediaDomain: 'http://goldpoisk.ru',
    mediaRoot: path.join(rootPath, '../../media/'),
    app: {
      name: 'backend'
    },
    port: 80,
    db: 'postgres://dev_goldpoisk:dev12345@goldpoisk.cy58blwqhzwr.eu-central-1.rds.amazonaws.com/production'
  }
};

module.exports = config[env];
