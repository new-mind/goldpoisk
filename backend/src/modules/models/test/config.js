// vim: ts=2: sw=2:
module.exports = {
  TEST_DB: 'postgres://postgres@localhost/goldpoisk_test',
  MEMORY_DB: 'sqlite://:memory:/test',
  opts: {
    logging: false
  }
}
