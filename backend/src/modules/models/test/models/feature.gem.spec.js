// vim: ts=2: sw=2:
var assert = require('assert');

var initDb = require('../../index');
var config = require('../config');

describe('Feature.Gem', function () {
  var db;

  before(function () {
    db = initDb(config.TEST_DB, config.opts);
  });

  it('Should return one gem', function (done) {
    db.Gem.model.findOne()
      .then(function (gem) {
        assert.equal(gem.name, 'Бриллиант', 'Should be right gem');
        done();
      }, done)
  })

  describe('Custom methods', function () {
    it('getForCategory', function (done) {
      db.Type.model.findOne({
        where: {
          id: 1
        }
      })
      .then(function (type) {
        db.Gem.getForCategory(type)
          .then(function (gems) {
            assert.equal(gems.length, 6, 'Should return right count gems');
            done();
          }, done)
          .catch(done);
      }, done)
    });
  });
});
