// vim: ts=2: sw=2:
var assert = require('assert');

var initDb = require('../../index');
var config = require('../config');

var PaginatorMock = {
  modelName: 'Product',
  currentPage: 1,
  totalPages: 1,
  sort: null,
  filter: null,
  itemsOnPage: 30,
  update: function () {},
  toJSON: function () {}
}

describe('Product', function () {
  var db;

  before(function () {
    db = initDb(config.TEST_DB, config.opts); 
  })

  it('Should return one product', function (done) {
    db.Product.model.findOne()
      .then(function () {
        assert(true);
        done();
      }, function (e) {
        done(e);
      })
  });

  describe('Instance methods', function () {
    it('hasAction', function (done) {
      getProduct(1)
      .then(function (product) {
        assert(product.hasAction(), 'Product should have action');
        done();
      })
      .catch(done);
    });

    it('isHit', function (done) {
      getProduct(3)
      .then(function (product) {
        assert(product.isHit(), 'Product should be hit');
        done();
      })
      .catch(done);
    });

    it('getShopName', function (done) {
      getProduct()
      .then(function (product) {
        var shopName = product.getShopName();
        assert.equal(shopName, 'moonlight', 'Should return right shop name');
        done();
      })
      .catch(done);
    });

    it('getShopUrl', function (done) {
      getProduct() 
      .then(function (product) {
        var shopName = product.getShopUrl();
        assert.equal(shopName, 'http://google.com', 'Should return right shop url');
        done();
      })
      .catch(done);
    });

    it('getUrl', function (done) {
      getProduct() 
      .then(function (product) {
        var url = product.getUrl();
        assert.equal(url, '/rings/kolco-2', 'Should return right product url');
        done();
      })
      .catch(done);
    });

    it('getBuyUrl', function (done) {
      getProduct() 
      .then(function (product) {
        var buyUrl = product.getBuyUrl();
        assert.equal(buyUrl, 'http://yandex.ru', 'Should return right buyUrl');
        done();
      })
      .catch(done);
    });

    it('count', function (done) {
      db.Product.model.findOne({
        include: [ db.Item.model ]
      })
      .then(function (product) {
        assert(product.count(), 'Should return count');
        done();
      }, done)
    });

    it(
    'getWeight', function (done) {
      getProduct(1)
      .then(function (product) {
        var weight = product.getWeight();
        assert.equal(weight, '1.2 гр.', 'Should return formated weight');
        done();
      })
      .catch(done);
    });

    it('getMainImage', function (done) {
      getProduct()
      .then(function (product) {
        assert(product.getMainImage(), 'Should return image');
        done();
      }, done)
    })
  });

  function getProduct (id) {
    id = id || 3;
    return db.Product.model.findOne({
      include: [{
        model: db.Item.model,
        include: [
          db.Shop.model,
          db.Action.model,
          db.Hit.model
        ]
      }, {
        model: db.Type.model
      }, {
        model: db.Image.model
      }],
      where: {
        id: id
      }
    })
  }

  describe('Custom methods', function () {
    it('getRangeCostForCategory', function (done) {
      db.Type.model.findOne({
        where: {id: 1}
      })


      .then(function (category) {
        db.Product.getRangeCostForCategory(category)
        .then(function ([min, max]) {
          assert.equal(min, 12.1, 'Should get min');
          assert.equal(max, 4100, 'Should get max');
          done();
        })
        .fail(done);
      })
      .catch(done);
    });

    it('getPage', function (done) {
      db.Type.model.findOne({
        where: {id: 1}
      })
      .then(function (category) {
        db.Product.getPage(category, PaginatorMock)
        .then(function () {
          assert(true);
          done();
        })
        .fail(done);
      })
      .catch(done);
    });

    it('getMany', function (done) {
      db.Product.getMany([1, 2])
      .then(function (products) {
        assert(true);
        done();
      })
      .fail(done);
    });

    it('getOne', function (done) {
      db.Product.getOne('kolco-1')
      .then(function (product) {
        assert(true);
        done();
      })
      .fail(done);
    });
  });
});
