// vim: ts=2: sw=2:
var assert = require('assert');

var initDb = require('../../index');
var config = require('../config');

describe('Type', function () {
  var db;

  before(function () {
    db = initDb(config.TEST_DB, config.opts);
  });

  it('Should return one type', function (done) {
    db.Type.model.findOne()
      .then(function () {
        assert(true);
        done();
      }, done)
  })

  describe('Instance methods', function () {
    it('getUrl', function (done) {
      db.Type.model.find({where: {id: 1}})
      .then(function (type) {
        assert(type.getUrl(), '/rings', 'Should be right path');
        done()
      }, done)
      .catch(done);
    })
  });

  describe('Custom methods', function () {
    it('getMenu', function (done) {
      db.Type.getMenu()
      .then(function () {
        assert(true)
        done();
      })
      .fail(done);
    });

  });
});
