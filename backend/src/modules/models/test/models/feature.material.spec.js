// vim: ts=2: sw=2:
var assert = require('assert');

var initDb = require('../../index');
var config = require('../config');

describe('Feature.Material', function () {
  var db;

  before(function () {
    db = initDb(config.TEST_DB, config.opts);
  });

  it('Should return one material', function (done) {
    db.Material.model.findOne({
      where: {
        id: 1
      }
    })
      .then(function (material) {
        assert.equal(material.name, 'Золото', 'Should be right material');
        done();
      }, done)
  })

  describe('Custom methods', function () {
    it('getForCategory', function (done) {
      db.Type.model.findOne({
        where: {
          id: 1
        }
      })
      .then(function (type) {
        db.Material.getForCategory(type)
          .then(function (materials) {
            assert.equal(materials.length, 2, 'Should return right count materials');
            done();
          }, done)
          .catch(done);
      }, done)
    });
  });
});
