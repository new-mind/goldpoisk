// vim: ts=2: sw=2:
var assert = require('assert');

var initDb = require('../../index');
var config = require('../config');

describe('Shop', function () {
  var db;

  before(function () {
    db = initDb(config.TEST_DB, config.opts);
  });

  it('Should return one shop', function (done) {
    db.Shop.model.findOne()
      .then(function () {
        assert(true);
        done();
      }, done)
  })

  describe('Custom methods', function () {
    it('getForCategory', function (done) {
      db.Type.model.findOne({
        where: {
          id: 1
        }
      })
      .then(function (type) {
        db.Shop.getForCategory(type)
          .then(function () {
            assert(true);
            done();
          }, done);
      }, done)
    });

  });
});
