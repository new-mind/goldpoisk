// vim: ts=2: sw=2:
var assert = require('assert');
var _ = require('lodash');

var initDb = require('../../index');
var config = require('../config');

describe('Validation Product model', function () {
  var db;
  var data = {
    name: 'Продукт',
    slug: 'product1',
    article: '1',
    weight: 2.1
  }

  beforeEach(function (done) {
    db = initDb(config.MEMORY_DB, config.opts);
    db.sequelize.sync()
      .then(function () {
        done();
      })
      .catch(function (e) {
        throw e;
      });
  });

  describe('#create', function () {
    it('successful', function (done) {
      db.Product.model.create(data)
      .then(function () {
        assert('Successfull');
      }, function (e) {
        assert(false, e);
      })
      .finally(done)
    });

    it('malformed name', function (done) {
      var product = _.assign({}, data);
      product.name = '1**23417>>>>';
      db.Product.model.create(product)
      .then(function () {
        assert(false, 'Should throw validation error');
      }, function (e) {
        assert(true);
      })
      .finally(done)
    });

    it('malformed slug', function (done) {
      var product = _.assign({}, data);
      product.slug = 'фырфыр';
      db.Product.model.create(product)
      .then(function () {
        assert(false, 'Should throw validation error');
      }, function (e) {
        assert(true);
      })
      .finally(done)
    });

    it('not unique slug', function (done) {
      var product = _.assign({}, data);
      product.number = '22';

      db.Product.model.create(data)
      .then(function () {
        db.Product.model.create(product)
          .then(function () {
            assert(false, 'Should throw validation error');
          }, function (e) {
            assert.equal(e.name, 'SequelizeUniqueConstraintError');
          })
          .finally(done);
      }, function (e) {
        assert(false, e);
        done();
      });
    });

    it('not unique article', function (done) {
      var product = _.assign({}, data);
      product.slug = '22bbs';

      db.Product.model.create(data)
      .then(function () {
        db.Product.model.create(product)
          .then(function () {
            assert(false, 'Should throw validation error');
          }, function (e) {
            assert.equal(e.name, 'SequelizeUniqueConstraintError');
          })
          .finally(done);
      }, function (e) {
        assert(false, e);
        done();
      });
    });

/*
    it('malformed weight', function (done) {
      var product = _.assign({}, data);
      product.weight = 'a';

      db.Product.model.create(product)
      .then(function () {
        assert(false, 'Should throw validation error');
      }, function (e) {
        assert(true);
      })
      .finally(done)
    });
    */

  });
});
