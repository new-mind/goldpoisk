// vim: ts=2: sw=2:
var assert = require('assert');
var _ = require('lodash');

var initDb = require('../../index');
var config = require('../config');

describe('Validation ProductType model', function () {
  var db;
  var data = {
    title: 'Кольца и Вася',
    name: 'rings',
    url: 'rings'
  }

  before(function (done) {
    db = initDb(config.MEMORY_DB, config.opts);
    db.sequelize.sync()
      .then(function () {
        done();
      })
      .catch(function (e) {
        throw e;
      });
  });

  describe('#create', function () {
    it('successful', function (done) {
      db.Type.model.create(data)
      .then(function () {
        assert('Successfull');
        done();
      }, done)
    });

    it('malformed name', function (done) {
      var type = _.assign({}, data);
      type.name = 'зязя';
      testUnvalid(type, done);
    });

    it('too short name', function (done) {
      var type = _.assign({}, data);
      type.name = 'ri';
      testUnvalid(type, done);
    });

    it('too long name', function (done) {
      var type = _.assign({}, data);
      type.name = 'ribokribokasdfasdftoo-xxxxxribokribokasdfasdftoo';
      testUnvalid(type, done);
    });

    it('malformed title', function (done) {
      var type = _.assign({}, data);
      type.title = 'Это кольца 228';
      testUnvalid(type, done);
    });

    it('too short title', function (done) {
      var type = _.assign({}, data);
      type.title = 'Эт';
      testUnvalid(type, done);
    });

    it('too long title', function (done) {
      var type = _.assign({}, data);
      type.title = 'abcdefgifhabcdefgifabcdefgifabcdefgifabcdefgifhabcdefgifhhhhvvv02s';
      testUnvalid(type, done);
    });

    it('malformed url', function (done) {
      var type = _.assign({}, data);
      type.url = 'Это урл';
      testUnvalid(type, done);
    });

    it('too short url', function (done) {
      var type = _.assign({}, data);
      type.url = 'ab';
      testUnvalid(type, done);
    });

    it('too long url', function (done) {
      var type = _.assign({}, data);
      type.url = 'urltoolong/urltoolong/lsag/asssfadiskxoallgia/sssss/abcdasdep';
      testUnvalid(type, done);
    });
  });

  function testUnvalid (data, done) {
    db.Type.model.create(data)
      .then(function () {
        done('Should throw validation error');
      }, function (e) {
        assert(true);
        done();
      })
  }
});
