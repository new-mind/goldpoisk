// vim: ts=2: sw=2:
var assert = require('assert');
var _ = require('lodash');

var initDb = require('../../index');
var config = require('../config');

describe('Validation Shop model', function () {
  var db; 
  var data = {
    name: 'Магазин у дяди Васи',
    url: 'http://yandex.ru',
    description: 'Я описание'
  }

  before(function (done) {
    db = initDb(config.MEMORY_DB, config.opts);
    db.sequelize.sync()
      .then(function () {
        done();
      })
      .catch(function (e) {
        throw e;
      });
  });

  describe('#create', function () {
    it('successful', function (done) {
      db.Shop.model.create(data)
      .then(function () {
        assert('Successfull');
        done();
      }, done)
    });

    it('malformed name', function (done) {
      var shop = _.assign({}, data);
      shop.name = '1**23417>>>>';
      testUnvalid(shop, done);
    });

    it('too short name', function (done) {
      var shop = _.assign({}, data);
      shop.name = '1';
      testUnvalid(shop, done);
    });

    it('malformed url', function (done) {
      var shop = _.assign({}, data);
      shop.url = 'http:/abc';
      testUnvalid(shop, done);
    });
  });

  function testUnvalid (data, done) {
    db.Shop.model.create(data)
      .then(function () {
        done('Should throw validation error');
      }, function (e) {
        assert(true);
        done();
      })
  }
});
