// vim: set ts=2: sw=2:
var fs = require('fs');
var path = require('path');

var Sequelize = require('sequelize');
/**
 * @param {String} url, postgres://url
 * @param {Object} db
 */
module.exports = function init (url, options) {
  var db = {};
  var sequelize = new Sequelize(url, options || {});

  var dirname = path.join(__dirname, './src');
  fs.readdirSync(dirname).filter(function (file) {
    var filePath = path.join(dirname, file);
    var isDir = fs.statSync(filePath).isDirectory();

    return !isDir && (file.indexOf('.') !== 0) && (file !== 'index.js');
  }).forEach(function (file) {
    var model = require('./src/' + file);
    model.init(db, sequelize, Sequelize);
    db[model.model.name] = model
  });

  Object.keys(db).forEach(function (modelName) {
    if ('associate' in db[modelName].model) {
      db[modelName].model.associate(db);
    }
  });

  db.sequelize = sequelize;
  db.Sequelize = Sequelize;

  return db;
};
