#!/bin/bash
# vim: ts=2: sw=2:

# Run from root
echo Preparation...
sh ./tools/create_test_db.sh 1>/dev/null

error=$?
if [ $error -ne 0 ]; then
  echo "Error during preparation: $error"
  exit 1
fi

echo Run
mocha --harmony_destructuring ./test/**/*.spec.js
