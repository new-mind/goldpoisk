#!/usr/bin
echo 'Create PL/PgSQL functions'
#production
USER=dev_goldpoisk
HOST=goldpoisk.cy58blwqhzwr.eu-central-1.rds.amazonaws.com
DB=production

root=`pwd $(dirname $BASH_SOURCE[0])`

files=$(ls `find $root/src/plpgsql -iname '*.sql'`)
psql="psql -U $USER -h $HOST $DB"

for f in $files; do
    echo "File: $f";
    sql=`cat $f`
    echo "$sql"
    $psql -c "$sql"
done;
