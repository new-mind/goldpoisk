#!/bin/bash
# vim: ts=2: sw=2:
echo 'Create test db'

DB=goldpoisk_test
HOST=localhost
USER=postgres

PSQL="psql -U $USER -h $HOST"
root=$(pwd `dirname BASH_SOURCE[0]`)

drop_database () {
    echo 'Drop db if exists'
    $PSQL -c "DROP DATABASE IF EXISTS $DB"
}

create_database () {
    echo 'Create new database'
    $PSQL -c "CREATE DATABASE $DB"
    echo 'Create tables'
    files=$(ls `find $root/src/schema/ -iname "*.sql"`)
    for f in $files; do
        execute $f
    done
}

populate_database () {
    echo 'Populate database'
    files=$(ls `find $root/fixtures/ -iname "*.sql"`)
    for f in $files; do
        execute $f
    done
}

execute () {
    sql=`cat $1`
    echo $1
    echo "$sql"
    $PSQL $DB -c "$sql"
}

drop_database
create_database
populate_database
