-- vim: ts=2: sw=2:
INSERT INTO feature
  (id, table_name)
VALUES
  (1, 'gem'),
  (2, 'gem'),
  (3, 'gem'),
  (4, 'gem'),
  (5, 'gem'),
  (6, 'gem'),
  (7, 'gem'),
  (8, 'material'),
  (9, 'material'),
  (10, 'material');
