-- vim: ts=2: sw=2:
INSERT INTO producttype
  (id, title, name, url)
VALUES
  (1, 'Кольца', 'rings', 'rings'),
  (2, 'Подвески', 'pendants', 'pendants'),
  (3, 'Серьги', 'earrings', 'earrings');
