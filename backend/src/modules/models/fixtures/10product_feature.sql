-- vim: ts=2: sw=2:
INSERT INTO product_feature
  (product_id, feature_id)
VALUES
  (1, 1),
  (1, 2),
  (2, 3),
  (2, 1),
  (2, 4),
  (2, 5),
  (2, 6),
  (6, 7),
  (1, 8),
  (2, 8),
  (3, 9),
  (4, 10);
