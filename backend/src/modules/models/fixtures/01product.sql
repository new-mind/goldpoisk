-- vim: ts=2: sw=2:
INSERT INTO product
  (id, type_id, article, name, slug, weight, created_at)
VALUES
  (1, 1, '123', 'Кольцо', 'kolco', 1.2, now()),
  (2, 1, '1234', 'Кольцо', 'kolco-1', 10.0, now()),
  (3, 1, '12', 'Кольцо', 'kolco-2', null, now()),
  (4, 2, '12345', 'Подвеска', 'podveska', null, now()),
  (5, 3, '12-34', 'Серьги', 'sergi', null, now()),
  (6, 3, '123-4', 'Ceрьги золотые', 'sergi-zolotie', null, now());
