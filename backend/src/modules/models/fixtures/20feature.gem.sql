-- vim: ts=2: sw=2:
INSERT INTO feature.gem
  (id, name, feature_id)
VALUES
  (1, 'Бриллиант', 1),
  (2, 'Рубин', 2),
  (3, 'Рубин обл.', 3),
  (4, 'Жемчуг', 4),
  (5, 'Речной жемчуг', 5),
  (6, 'Активированный уголь', 6),
  (7, 'Стекло', 7);
