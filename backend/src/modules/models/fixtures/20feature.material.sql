-- vim: ts=2: sw=2:
INSERT INTO feature.material
  (id, name, feature_id)
VALUES
  (1, 'Золото', 8),
  (2, 'Серебро', 9),
  (3, 'Стекло', 10);
