-- vim: ts=2: sw=2:
INSERT INTO item
  (id, product_id, shop_id, cost, quantity, buy_url)
VALUES
  (1, 1, 1, 1000, 2, 'http://qupi-menya'),
  (2, 1, 2, 1100, 1, 'http://qupi-menya/tut'),
  (3, 2, 1, 4100, 1, 'http://qupi-menya/tam'),
  (4, 3, 2, 12.100, 1, 'http://yandex.ru'),
  (5, 4, 2, 11.01, 5, 'http://ne-pokupai'),
  (6, 5, 1, 20, 0, 'https://secure/buy');
