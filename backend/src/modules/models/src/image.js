// vim: ts=2: sw=2:
var sprintf = require('sprintf');

var MEDIA_DOMAIN = process.env.MEDIA_DOMAIN;

module.exports = {
  init: function (db, sequelize, DataTypes) {
    this.db = db;

    var Image = this.model = sequelize.define('Image', {
      src: DataTypes.STRING,
      main: DataTypes.BOOLEAN
    }, {
      tableName: 'image',
      createdAt: false,
      updatedAt: false,
      instanceMethods: {
        getUrl: function () {
          return sprintf('%s/media/%s', MEDIA_DOMAIN, this.src);
        },

        toJSON: function () {
          return this.getUrl()
        }
      }
    });

    return Image;
  },

  db: null,
  model: null
}
