// vim: ts=2: sw=2:
var Q = require('q');
var sprintf = require('sprintf');
var _ = require('lodash');

module.exports = {
  init: function (db, sequelize, DataTypes) {
    this.db = db;
    var BestBid = this.model = sequelize.define('BestBid', {}, {
      tableName: 'cms_bestbid',
      createdAt: false,
      updatedAt: false,
      classMethods: {
        associate: function (models) {
          BestBid.belongsTo(models.Item.model, {
            foreignKey: 'item_id'
          });
        }
      }
    });

    return this.model;
  },

  /**
   *  Description
   *
   *  @return {Object[]}
   **/
  getMany: function (limit) {
    var self = this;
    var db = this.db;

    return Q.Promise(function (resolve, reject) {

      self.model.findAndCountAll({
        limit: limit,
        include: [{
          model: db.Item.model,
          required: true,
          include: [{
            model: db.Product.model,
            required: true
          }]
        }]
      })
      .then(function (data) {
        var ids = _.map(data.rows, function (row) {
          return row.Item.Product.id;
        });
        Q.all([
          db.Product.getMany(ids)
        ])
        .spread(function (products) {
          resolve([data.count, products])
        })
        .fail(reject);

      }, reject);

    });
  },

  db: null,
  model: null
};
