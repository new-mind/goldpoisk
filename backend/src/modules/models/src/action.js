// vim: ts=2: sw=2:
var Q = require('q');

module.exports = {
  init: function (db, sequelize, DataTypes) {
    this.db = db;
    this.model = sequelize.define('Action', {}, {
      tableName: 'action',
      createdAt: false,
      updatedAt: false
    })
    return this.model;
  },

  db: null,
  model: null
}
