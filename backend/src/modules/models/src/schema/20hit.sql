-- vim: ts=2: sw=2:
-- Table: hit

-- DROP TABLE hit;

CREATE TABLE hit
(
  id serial NOT NULL,
  item_id integer NOT NULL,
  CONSTRAINT hit__id PRIMARY KEY (id),
  CONSTRAINT hit__item_id0fk FOREIGN KEY (item_id)
      REFERENCES item (id) MATCH SIMPLE,
  CONSTRAINT hit__item_id0unique UNIQUE (item_id)
);
