-- vim: ts=2: sw=2:
-- Table: shop

-- DROP TABLE shop;

CREATE TABLE shop
(
  id serial NOT NULL,
  name character varying(255) NOT NULL,
  url character varying(128) NOT NULL,
  description text,
  created_at timestamp without time zone DEFAULT now(),
  CONSTRAINT shop__id PRIMARY KEY (id),
  CONSTRAINT shop__name0unique UNIQUE (name),
  CONSTRAINT shop__url0unique UNIQUE (url)
);
