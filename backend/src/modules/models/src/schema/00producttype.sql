-- vim: ts=2: sw=2:
-- Table: producttype

-- DROP TABLE producttype;

CREATE TABLE producttype
(
  id serial NOT NULL,
  title character varying(64) NOT NULL,
  name character varying(32) NOT NULL,
  url character varying(32) NOT NULL,
  CONSTRAINT producttype__id PRIMARY KEY (id),
  CONSTRAINT producttype__name0unique UNIQUE (name),
  CONSTRAINT producttype__url0unique UNIQUE (url)
);
