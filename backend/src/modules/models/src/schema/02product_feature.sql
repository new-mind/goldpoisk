-- vim: ts=2: sw=2:
-- Table: product_feature

-- DROP TABLE product_feature;

CREATE TABLE product_feature
(
  product_id integer,
  feature_id integer,
  CONSTRAINT product_feature__feature_id0fk FOREIGN KEY (feature_id)
      REFERENCES feature (id) MATCH SIMPLE,
  CONSTRAINT product_feature__product_id0fk FOREIGN KEY (product_id)
      REFERENCES product (id) MATCH SIMPLE
)
WITH (
  OIDS=FALSE
);
