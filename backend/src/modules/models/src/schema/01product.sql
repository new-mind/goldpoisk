-- vim: ts=2: sw=2:
-- Table: product

-- DROP TABLE product;

CREATE TABLE product
(
  id serial NOT NULL,
  type_id integer NOT NULL,
  article character varying(255) NOT NULL,
  name character varying(255) NOT NULL,
  slug character varying(255) NOT NULL,
  weight numeric(8,5),
  description text,
  created_at timestamp without time zone DEFAULT now(),
  CONSTRAINT product__id PRIMARY KEY (id),
  CONSTRAINT product__type_id0fk FOREIGN KEY (type_id)
      REFERENCES producttype (id) MATCH SIMPLE,
  CONSTRAINT product__article0unique UNIQUE (article),
  CONSTRAINT product__slug0unique UNIQUE (slug)
)
WITH (
  OIDS=FALSE
);
