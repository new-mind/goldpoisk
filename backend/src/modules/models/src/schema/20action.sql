-- vim: ts=2: sw=2:
-- Table: action

-- DROP TABLE action;

CREATE TABLE action
(
  id serial NOT NULL,
  item_id integer NOT NULL,
  CONSTRAINT action_id PRIMARY KEY (id),
  CONSTRAINT action__item_id0fk FOREIGN KEY (item_id)
      REFERENCES item (id) MATCH SIMPLE,
  CONSTRAINT action__item_id0unique UNIQUE (item_id)
);
