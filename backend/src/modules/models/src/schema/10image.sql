-- vim: ts=2: sw=2:
-- Table: image

-- DROP TABLE image;

CREATE TABLE image
(
  id serial NOT NULL,
  product_id integer NOT NULL,
  src character varying(160) NOT NULL,
  main boolean DEFAULT false,
  CONSTRAINT image__id PRIMARY KEY (id),
  CONSTRAINT image__product_id0fk FOREIGN KEY (product_id)
      REFERENCES product (id) MATCH SIMPLE,
  CONSTRAINT image__id_main0unique UNIQUE (id, main),
  CONSTRAINT image__src0unique UNIQUE (src)
);
