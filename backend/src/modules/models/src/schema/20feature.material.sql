-- vim: ts=2: sw=2:
-- Table: feature.material

-- DROP TABLE feature.material;

CREATE TABLE feature.material
(
  id serial NOT NULL,
  name character varying(128),
  feature_id integer,
  CONSTRAINT material__id PRIMARY KEY (id),
  CONSTRAINT material__feature_id0fk FOREIGN KEY (feature_id)
      REFERENCES feature (id) MATCH SIMPLE,
  CONSTRAINT material__name0unique UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
