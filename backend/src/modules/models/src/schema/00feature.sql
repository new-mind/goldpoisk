-- vim: ts=2: sw=2:
-- Table: feature

-- DROP TABLE feature;

CREATE TABLE feature
(
  id serial NOT NULL,
  table_name character varying(32) NOT NULL,
  CONSTRAINT feature__id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
