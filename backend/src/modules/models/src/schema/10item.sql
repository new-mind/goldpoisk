-- vim: ts=2: sw=2:
-- Table: item

-- DROP TABLE item;

CREATE TABLE item
(
  id serial NOT NULL,
  product_id integer NOT NULL,
  shop_id integer NOT NULL,
  cost double precision,
  quantity numeric DEFAULT 0,
  buy_url character varying(256) NOT NULL,
  created_at timestamp without time zone DEFAULT now(),
  updated_at timestamp without time zone DEFAULT now(),
  CONSTRAINT item__id PRIMARY KEY (id),
  CONSTRAINT item__buy_url0unique UNIQUE (buy_url),
  CONSTRAINT item__product_id0fk FOREIGN KEY (product_id)
      REFERENCES product (id) MATCH SIMPLE,
  CONSTRAINT item__shop_id0fk FOREIGN KEY (shop_id)
      REFERENCES shop (id) MATCH SIMPLE
);
