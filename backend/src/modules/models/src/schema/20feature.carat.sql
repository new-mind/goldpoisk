-- vim: ts=2: sw=2:
-- Table: feature.carat

CREATE SCHEMA IF NOT EXISTS feature;

-- DROP TABLE feature.carat;

CREATE TABLE feature.carat
(
  id serial NOT NULL,
  value double precision NOT NULL,
  feature_id integer,
  CONSTRAINT carat_pkey PRIMARY KEY (id),
  CONSTRAINT carat__feature_id0fk FOREIGN KEY (feature_id)
      REFERENCES feature (id) MATCH SIMPLE
)
WITH (
  OIDS=FALSE
);
