-- vim: ts=2: sw=2:
-- Table: cms_bestbid

-- DROP TABLE cms_bestbid;

CREATE TABLE cms_bestbid
(
  id serial NOT NULL,
  item_id integer NOT NULL,
  CONSTRAINT cms_bestbid__id PRIMARY KEY (id),
  CONSTRAINT cms_bestbid__item_id0fk FOREIGN KEY (item_id)
      REFERENCES item (id) MATCH SIMPLE,
  CONSTRAINT cms_bestbid__item_id0unique UNIQUE (item_id)
)
WITH (
  OIDS=FALSE
);
