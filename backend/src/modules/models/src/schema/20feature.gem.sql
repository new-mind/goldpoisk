-- vim: ts=2: sw=2:
-- Table: feature.gem

CREATE SCHEMA IF NOT EXISTS feature;

-- DROP TABLE feature.gem;

CREATE TABLE feature.gem
(
  id serial NOT NULL,
  name character varying(255),
  feature_id integer,
  CONSTRAINT gem__id PRIMARY KEY (id),
  CONSTRAINT gem__feature_id0fk FOREIGN KEY (feature_id)
      REFERENCES feature (id) MATCH SIMPLE
)
WITH (
  OIDS=FALSE
);
