// vim: ts=2: sw=2:
module.exports = {
  init: function (db, sequelize, DataTypes) {
    this.db = db;
    var Promotion = this.model = sequelize.define('Promotion', {
      x: DataTypes.DECIMAL,
      y: DataTypes.DECIMAL
    }, {
      tableName: 'cms_promotion',
      createdAt: false,
      updatedAt: false,
      classMethods: {
        associate: function (models) {
          Promotion.belongsTo(models.Banner.model, {
            foreignKey: 'banner_id'
          });
          Promotion.belongsTo(models.Item.model, {
            foreignKey: 'item_id'
          });
        }
      }
    });

    return Promotion;
  },

  db: null,
  model: null
};
