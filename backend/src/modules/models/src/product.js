// vim: ts=2: sw=2:
var Q = require('q');
var _ = require('lodash');
var sprintf = require('sprintf');

var isAlphanumeric = require('./utils/validation').isAlphanumeric;

module.exports = {
  init: function (db, sequelize, DataTypes) {
    this.db = db;
    this.model = sequelize.define('Product', {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          isAlphanumeric: isAlphanumeric,
          isLength: {
            min: 3,
            max: 128
          }
        }
      },
      description: DataTypes.STRING,
      slug: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isAscii: true,
          isLength: {
            min: 3,
            max: 128
          }
        }
      },
      article: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      weight: {
        type: DataTypes.DECIMAL,
        validate: {
          isDecimal: true
        }
      }
    }, {
      tableName: 'product',
      createdAt: 'created_at',
      updatedAt: false,
      instanceMethods: {
        hasAction: function () {
          return _.find(this.get('Items'), function (item) {
            return item.Actions.length;
          })
        },

        isHit: function () {
          return _.find(this.get('Items'), function (item) {
            return item.Hits.length;
          })
        },

        getShopName: function () {
          var count = this.count();
          if (count > 1 || !count)
            return null;

          var shop = this.get('Items')[0].Shop;
          return shop.name;
        },

        getShopUrl: function () {
          var count = this.count();
          if (count > 1 || !count)
            return;

          var shop = this.get('Items')[0].Shop;
          return shop.url;
        },

        getBuyUrl: function () {
          var count = this.count();
          if (count > 1 || !count)
            return null;

          return this.get('Items')[0].buy_url;
        },

        count: function () {
          return _.filter(this.get('Items'), function (item) {
            //TODO:
            return true || item.quantity;
          }).length;
        },

        getUrl: function () {
          var typeUrl = this.get('typeurl');
          // get from included model
          if (!typeUrl) {
            typeUrl = this.get('Type').url;
          }

          return sprintf('/%s/%s', typeUrl, this.slug);
        },

        getWeight: function () {
          //TODO:
          if (!this.weight)
            return '0 гр.';
          return sprintf('%f гр.', this.weight);
        },

        getMainImage: function () {
          var Images = this.get('Images');
          var image = Images && Images[0];
          return image ? image.getUrl() : '/404'
        },

        toJSON: function () {
          var url = this.getUrl();
          return {
            id: this.id,
            title: this.name,
            number: this.article,
            url: url,
            minPrice: this.get('min'),
            maxPrice: this.get('max'),
            jsonUrl: sprintf('/rest%s', url),
            weight: this.getWeight(),
            image: this.getMainImage(),
            count: this.count(),
            action: this.hasAction(),
            hit: this.isHit(),
            buyUrl: this.getBuyUrl(),
            shopName: this.getShopName(),
            shopUrl: this.getShopUrl()
          }
        },

        toJSON2: function () {
          var url = this.getUrl();
          return {
            id: this.id,
            title: this.name,
            url: url,
            number: this.article,
            weight: this.getWeight(),
            images: this.Images,
            items: this.Items,
            //TODO: delete
            gems: [],
            action: this.hasAction(),
            hit: this.isHit(),
          }
        }
      },

      classMethods: {
        associate: function (db) {
          this.belongsTo(db.Type.model, {
            foreignKey: 'type_id'
          });

          this.hasMany(db.Item.model, {
            foreignKey: 'product_id'
          });

          this.hasMany(db.Image.model, {
            foreignKey: 'product_id'
          });
        }
      }
    });

    return this.model;
  },

  getRangeCostForCategory: function getRangeCostForCategory (category) {
    var db = this.db;
    var sequelize = db.sequelize;
    console.time('Product.getRangeCostForCategory');

    return Q.Promise(function (resolve, reject) {
      sequelize.query(
        'SELECT min(cost), max(cost) FROM item ' +
        'WHERE product_id IN ' +
          '(SELECT id from product WHERE ' +
            'product.type_id = ?);'
      , {
        replacements: [category.id]
      })
        .spread(function ([costRange]) {
          console.timeEnd('Product.getRangeCostForCategory');
          resolve([costRange.min, costRange.max])
        }, reject);
    });
  },

  /**
   * Return promise with lists of products on the page
   *
   * @param {Object} category, instance db.Type.model
   * @param {Object} paginator, instance store.paginator.Paginator
   * @return {Object} promise
   */
  getPage: function getPage (category, paginator) {
    var self = this;
    var db = this.db;
    var sequelize = db.sequelize;

    var countQuery = this._getCountQuery(paginator);
    var dataQuery = this._getDataQuery(paginator);
    var params = this._getQueryParams(category, paginator);

    console.time('Product.getPage');
    return Q.Promise(function (resolve, reject) {
      sequelize.transaction(function (t) {
        return Q.all([
          sequelize.query(countQuery, {
              transaction: t,
              replacements: params,
              plain: true
          }),
          sequelize.query(dataQuery, {
            transaction: t,
            replacements: params,
            model: db.Product.model
          })
        ]);
      })
      .spread(function (select, products) {
        var count = parseInt(select.count, 10);

        Q.all([
          self._queryImages(products),
          self._queryItems(products)
        ])
        .spread(function (images, items) {
          self._populateWithImages(products, images);
          self._populateWithItems(products, items);
          console.timeEnd('Product.getPage');
          paginator.update(count);
          resolve([count, products]);
        })
        .fail(reject);
      })
      .catch(reject);
    });
  },

  /**
   * Return lists of products by ids
   *
   * @param {Number[]} ids
   * @return {Object} promise
   *   @resolve {Object[]} products
   *   @reject {Object} error
   *
   */
  getMany: function getMany (ids) {
    var self = this;
    var db = this.db;
    var sequelize = db.sequelize;

    var whereSQL = 'WHERE p.id in (:ids)'
    var query = this._getCommonQuery(null, whereSQL);
    var params = {
      ids: _.map(ids, function (id) { return +id; })
    }

    console.time('Product.getMany');
    return Q.Promise(function (resolve, reject) {
      sequelize.query(query, {
        replacements: params,
        model: db.Product.model
      })
      .then(function (products) {
        Q.all([
          self._queryImages(products),
          self._queryItems(products)
        ])
        .spread(function (images, items) {
          self._populateWithImages(products, images);
          self._populateWithItems(products, items);
          console.timeEnd('Product.getMany');
          resolve(products);
        })
        .fail(reject);
      }, reject)
    });
  },

  getOne: function getOne (slug) {
    var self = this;
    var db = this.db;
    var sequelize = db.sequelize;


    return Q.Promise(function (resolve, reject) {

      self.model.findOne({
        where: {
          slug: slug
        },
        include: [{
          model: db.Item.model,
          required: true,
          include: [{
              model: db.Shop.model,
              required: true
            },
            db.Action.model,
            db.Hit.model
          ]}, {
            model: db.Image.model
          },
          db.Type.model
        ]
      })
        .then(function (product) {
          resolve(product)
        }, reject);
    });
  },

  _queryItems: function Product__queryItems (products) {
    var db = this.db;

    return Q.Promise(function (resolve, reject) {
      if (!products.length)
        return resolve(null);

      db.Item.model.findAll({
        where: {
          product_id: {
            $in: _.map(products, 'id')
          }
        },

        include: [
          db.Action.model,
          db.Hit.model,
          db.Shop.model
        ]
      })
      .then(resolve, reject);
    })
  },

  _queryImages: function Product__queryImages (products) {
    var db = this.db;

    return Q.Promise(function (resolve, reject) {
      if (!products.length)
        return resolve(null);

      db.Image.model.findAll({
        where: {
          product_id: {
            $in: _.map(products, 'id')
          }
        },
        order: ['id']
      })
      .then(resolve, reject)
    });
  },

  _getCommonQuery: function Product__getCommonQuery (paginator, whereSQL) {
    whereSQL = whereSQL || 'WHERE p.type_id = :id';
    var commonQuery = [
      'SELECT p.id, p.name, p.article, p.slug, p.weight,',
      'MAX(i.cost) AS min, MIN(i.cost) AS max,',
      't.url AS typeurl',
      'FROM product AS p',
      'INNER JOIN item AS i ON i.product_id = p.id',
      'INNER JOIN producttype AS t ON p.type_id = t.id',
      paginator && paginator.filter ? paginator.filter.getSQL() : '',
      whereSQL,
      'GROUP BY p.id, typeurl',
      paginator && paginator.sort ? paginator.sort.getSQL() : ''
    ].join(' ');

    return commonQuery;
  },

  _getCountQuery: function Product__getCountQuery (paginator) {
    var commonQuery = this._getCommonQuery(paginator);
    var countQuery =
      sprintf('SELECT DISTINCT COUNT(*) FROM (%s) AS p;', commonQuery);

    return countQuery;
  },

  _getDataQuery: function Product__getDataQuery (paginator) {
    var commonQuery = this._getCommonQuery(paginator);
    var dataQuery =
      'SELECT * FROM (%s) AS p ' +
      'LIMIT :limit ' +
      'OFFSET :offset;'
    dataQuery = sprintf(dataQuery, commonQuery);
    return dataQuery;
  },

  _getQueryParams: function Product__getQueryParams (category, paginator) {
    var currentPage = paginator.currentPage - 1;
    var itemsOnPage = paginator.itemsOnPage;
    var filterValues = paginator.filter ? paginator.filter.getValues() : {};

    var params = {
      id: category.id,
      materials: filterValues.material,
      gems: filterValues.gem,
      shops: filterValues.shop,
      maxCost: filterValues.maxCost,
      minCost: filterValues.minCost,
      limit: itemsOnPage,
      offset: currentPage * itemsOnPage
    }
    return params;
  },

  _populateWithImages: function Product__populateWithImages (products, images) {
    _.forEach(products, function (product) {
      var Images = _.filter(images, function (image) {
        return product.id == image.product_id
      });

      product.setDataValue('Images', Images);
    })
  },

  _populateWithItems: function Product__populateWithItems (products, items) {
    _.forEach(products, function (product) {
      var Items = _.filter(items, function (item) {
        return product.id == item.product_id
      });


      product.setDataValue('Items', Items);
    })
  },

  db: null,
  model: null
}
