// vim: ts=2: sw=2:
var fs = require('fs');
var Q = require('q');

module.exports = {
  init: function (db, sequelize, DataTypes) {
    this.db = db;
    var Item = this.model = sequelize.define('Item', {
      cost: DataTypes.INTEGER,
      quantity: DataTypes.INTEGER,
      buy_url: {
        type: DataTypes.STRING(256),
        validate: {
          isUrl: true
        }
      }
    }, {
      tableName: 'item',
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      instanceMethods: {
        toJSON: function () {
          return {
            price: this.cost,
            quantity: this.quantity,
            buyUrl: this.buy_url,
            storeName: this.Shop.name,
            storeUrl: this.Shop.url
          }
        }
      },
      classMethods: {
        associate: function (db) {
          this.belongsTo(db.Product.model, {
            foreignKey: 'product_id'
          });

          this.belongsTo(db.Shop.model, {
            foreignKey: 'shop_id'
          });

          this.hasMany(db.Action.model, {
            foreignKey: 'item_id'
          });

          this.hasMany(db.BestBid.model, {
            foreignKey: 'item_id'
          });

          this.hasMany(db.Hit.model, {
            foreignKey: 'item_id'
          });
        }
      }
    });

    return Item;
  },

  getItem: function (id) {
    var d = Q.defer();

    fs.readFile('api/common/examples/item/200_response.json', (err, data) => {
      if (err)
        d.reject(err);
      d.resolve(JSON.parse(data).item);
    });

    return d.promise;
  },

  db: null,
  model: null
}
