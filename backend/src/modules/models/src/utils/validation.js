// vim: ts=2: sw=2:
var ValidationError = require('sequelize').ValidationError;

module.exports = {
  isAlpha: function (value) {
    var isValid = /^[a-zа-я\s]+$/i.test(value);
    if (!isValid)
      throw new ValidationError('Not alpha');
  },

  isAlphanumeric: function (value) {
    var isValid = /^[a-zа-я\d\s-_]+$/i.test(value);
    if (!isValid)
      throw new ValidationError('Not alphanumeric');
  }
}
