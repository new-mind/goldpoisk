// vim: ts=2: sw=2:
var Q = require('q');

var isAlphanumeric = require('./utils/validation').isAlphanumeric;

module.exports = {
  init: function (db, sequelize, DataTypes) {
    this.db = db;
    this.model = sequelize.define('Shop', {
      name: {
        type: DataTypes.STRING(255),
        validate: {
          isAlphanumeric: isAlphanumeric,
          isLength: {
            min: 3,
            max: 255
          }
        }
      },
      url: {
        type: DataTypes.STRING(128),
        validate: {
          isUrl: true
        }
      },
      description: DataTypes.STRING
    }, {
      tableName: 'shop',
      createdAt: 'created_at',
      updatedAt: false
    })

    return this.model
  },

  getForCategory: function getForCategory (category) {
    var db = this.db;
    var sequelize = db.sequelize;
    console.time('Shop.getForCategory');

    return Q.Promise(function (resolve, reject) {
      sequelize.query(
        'SELECT shop.id, shop.name FROM shop ' +
        'INNER JOIN ' +
          '(item INNER JOIN ' +
            '(SELECT id FROM product WHERE ' +
              'product.type_id = ?) AS product ' +
            'ON item.product_id = product.id) ' +
         'ON shop.id = item.shop_id ' +
         'GROUP BY shop.id, shop.name;'
      , {
        replacements: [category.id]
      })
        .spread(function (shops) {
          console.timeEnd('Shop.getForCategory');
          resolve(shops);
        }, reject);
    });
  },

  db: null,
  model: null
}
