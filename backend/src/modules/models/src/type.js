// vim: ts=2: sw=2
var Q = require('q');
var _ = require('lodash');
var sprintf = require('sprintf');

var utils = require('./utils/validation');

// order in menu
var CATEGORIES = [
  'rings',
  'earrings',
  'necklaces',
  'chains',
  'pendants',
  'bracelets',
  'brooches',
  'watches',
//  'cutlery',
]

module.exports = {
  init: function (db, sequelize, DataTypes) {
    this.db = db;
    var Type = this.model = sequelize.define('Type', {
      title: {
        type: DataTypes.STRING(64),
        validate: {
          isAlpa: utils.isAlpha,
          isLength: {
            min: 3,
            max: 64
          }
        }
      },
      name: {
        type: DataTypes.STRING(32),
        validate: {
          isAlphanumeric: 'en-US',
          isLength: {
            min: 3,
            max: 32
          }
        }
      },
      url: {
        type: DataTypes.STRING(32),
        validate: {
          isAscii: true,
          isLength: {
            min: 3,
            max: 32
          }
        }
      }
    }, {
      tableName: 'producttype',
      createdAt: false,
      updatedAt: false,
      instanceMethods: {
        getUrl: function () {
          return sprintf('/%s', this.url);
        }
      }
    });

    return Type;
  },

  /**
   * @param {String} category
   * @param {Boolean} fromCache
   * @return {Array} menu
   */
  getMenu: function getMenu (category, fromCache) {
    var self = this;

    if (fromCache && this._menu)
      return Q(this._menu);

    return Q.Promise(function (resolve, reject) {
      self.model.findAll({order: 'id'}).then(function (types) {
          var menu = self._sort(types);
          self._menu = _.map(menu, function (item) {
              return {
                "href": item.url,
                "type": item.name,
                "label": item.title
              }
          });

          resolve(self._menu);
      }, reject)
      .catch(reject);
    });
  },

  /**
   * @return {String[]} categories
   */
  getCategories: function getCategories () {
    return CATEGORIES;
  },

  /**
   * Sort menu as in CATEGORIES
   */
  _sort: function sort (menu) {
    var list = [];
    var categories = this.getCategories();

    categories.forEach(function (category) {
      var type = _.find(menu, function (type) {
        return type.name == category;
      })
      if (!type) {
        console.error('Cannot find category', category);
        return;
      }

      list.push(type);
    })
    return list;
  },

  db: null,
  model: null,

  _menu: null
}
