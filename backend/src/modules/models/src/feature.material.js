// vim: ts=2: sw=2:
var Q = require('q');
var _ = require('lodash');

module.exports = {
  init: function (db, sequelize, DataTypes) {
    this.db = db;
    var Material = this.model = sequelize.define('Material', {
      name: {
        type: DataTypes.STRING(128),
        allowNull: false,
        unique: true
      }
    }, {
      schema: 'feature',
      tableName: 'material',
      createdAt: false,
      updatedAt: false,
    });

    return Material;
  },

  getForCategory: function getForCategory (category) {
    var db = this.db;
    var sequelize = db.sequelize;

    var sql = [
      'SELECT DISTINCT m.id, m.name FROM feature.material AS m',
      'INNER JOIN feature',
      'ON feature.id = m.feature_id',

      'INNER JOIN product_feature AS pf',
      'ON feature.id = pf.feature_id',

      'INNER JOIN product',
      'ON product.id = pf.product_id',
      'WHERE product.type_id = ?'
    ].join(' ');

    console.time('Material.getForCategory');
    return Q.Promise(function (resolve, reject) {
      sequelize.query(sql, {
        replacements: [category.id],
        logging: true
      })
        .spread(function (materials) {
          console.timeEnd('Material.getForCategory');
          resolve(materials);
        }, reject);
    });
  },

  db: null,
  model: null
}
