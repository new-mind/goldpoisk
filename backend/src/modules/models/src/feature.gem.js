// vim: ts=2: sw=2:
var Q = require('q');
var _ = require('lodash');

module.exports = {
  init: function (db, sequelize, DataTypes) {
    this.db = db;
    var Gem = this.model = sequelize.define('Gem', {
        name: {
          type: DataTypes.STRING(255),
          allowNull: false,
          unique: true
        }
    }, {
        schema: 'feature',
        tableName: 'gem',
        createdAt: false,
        updatedAt: false
    });

    return Gem;
  },

  getForCategory: function getForCategory (category) {
    var db = this.db;
    var sequelize = db.sequelize;
    console.time('Gem.getForCategory')

    var sql = [
      'SELECT DISTINCT gem.id, gem.name FROM feature.gem AS gem',
      'INNER JOIN feature',
      'ON feature.id = gem.feature_id',

      'INNER JOIN product_feature AS pf',
      'ON feature.id = pf.feature_id',

      'INNER JOIN product',
      'ON product.id = pf.product_id',
      'WHERE product.type_id = ?'
    ].join(' ');

    return Q.Promise(function (resolve, reject) {
      sequelize.query(sql , {
        replacements: [category.id]
      })
        .spread(function (gems) {
          console.timeEnd('Gem.getForCategory');
          resolve(gems);
        }, reject);
    });
  },

  db: null,
  model: null
}
