// vim: ts=2: sw=2:
var Q = require('q');
var sprintf = require('sprintf');

var MEDIA_DOMAIN = process.env.MEDIA_DOMAIN;

module.exports = {
  init: function (db, sequelize, DataTypes) {
    this.db = db;
    var Banner = this. model = sequelize.define('Banner', {
      name: DataTypes.STRING(128),
      image: DataTypes.STRING(128),
      hidden: DataTypes.BOOLEAN
    }, {
      tableName: 'cms_banner',
      createdAt: false,
      updatedAt: false,
      classMethods: {
        associate: function (models) {
          Banner.hasMany(models.Promotion.model, {
            foreignKey: 'banner_id'
          })
        }
      }
    });

    return Banner;
  },

  getPage: function () {
    var self = this;
    var d = Q.defer();

    this.model.findAll({
      include: [{ all: true, nested: true }]
    }).then(function (banners) {
      d.resolve(banners.map(self._mapBanner, self));
    }, function (err) {
      d.reject(err);
    });

    return d.promise;
  },

  _mapBanner: function _mapBanner (banner) {
    var src = sprintf('%s/media/%s', MEDIA_DOMAIN, banner.image)

    return {
      src: src,
      title: banner.name,
      items: banner.Promotions.map(this._mapItem)
    };
  },

  _mapItem: function _mapItem (promoItem) {
    return {
      title: promoItem.Item.Product.name,
      price: promoItem.Item.cost,
      url: promoItem.Item.Product.getUrl(),
      x: promoItem.x,
      y: promoItem.y
    };
  },

  db: null,
  model: null
};
