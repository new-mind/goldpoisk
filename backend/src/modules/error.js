/**
 * ServerError(status, message, stack)
 * ServerError(status, @Error)
 * ServerError(status, @ServerError)
 */
function ServerError (status, message, stack) {
    if (message instanceof Error) {
        return new ServerErrorFromOne(status, message);
    }

    if (message instanceof ServerError) {
        return new ServerErrorFromError(status, message);
    }
    this.status = status;
    this.message = message || 'Unknown error';
    this.message += '';
    this.stack = stack || '';
    return this;
}

function ServerErrorFromOne (status, serverError) {
    return new ServerError(status, serverError.message, serverError.stack);
}

function ServerErrorFromError (status, error) {
    return new ServerError(status, error.message, error.stack);
}

ServerError.prototype = Error.prototope;

function ServerError404 (message) {
    return new ServerError(404, message);
}

function ServerError500 (message) {
    return new ServerError(500, message);
}

module.exports = {
    ServerErrorFromError: ServerErrorFromError,
    ServerError: ServerError,
    '404': ServerError404,
    '500': ServerError500
}
