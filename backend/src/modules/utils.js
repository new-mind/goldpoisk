// vim: ts=2: sw=2:
var ServerError500 = require('./error')['500'];
var ServerError404 = require('./error')['404'];
var ServerErrorFromError = require('./error')['ServerErrorFromError'];

module.exports = {
  getErrorHandlers: function (res, next) {
    return [
      function send200 (html) {
        res.send(html);
      },

      function send500 (err) {
        if (typeof error == 'string')
            next(ServerError500(err));
        else
            next(ServerErrorFromError(500, err));
      }
    ]
  }
}
