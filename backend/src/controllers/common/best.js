// vim: ts=2: sw=2:
var engine = require('express-bemhtml-priv.js');
var Q = require('q');

var models = require('../../db');
var utils = require('../../modules/utils');
var PaginatorFromRequest = require('../../stores/paginator').createFromRequest;

var env = {};
module.exports = function Best (req, res, next) {
  var paginator = PaginatorFromRequest('Product', req);
  var [send200, send500] = utils.getErrorHandlers(res, next);

  Q.all([
    models.Type.getMenu(),
    models.BestBid.getMany(),
    paginator.filter.load('best')
  ])
  .spread(function (menu, [count, products]) {
    var context = {
      menu: menu,
      products: products,
      category: "Лучшие предложения",
      count: count,
      sortParams: paginator.sort.toJSON(),
      paginator: paginator.toJSON('bids', req.path),
      filters: paginator.filter.toJSON()
    };

    if (req.xhr) {
      return send200(context);
    }

    engine.render('pages["category"]', [context, env])
      .then(send200)
      .fail(send500);
  })
  .fail(send500);
};
