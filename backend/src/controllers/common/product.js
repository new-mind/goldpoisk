// vim: ts=2: sw=2:
var engine = require('express-bemhtml-priv.js');
var Q = require('q');

var models = require('../../db');
var utils = require('../../modules/utils');
var ServerError404 = require('../../modules/error')['404'];

var env = {};
module.exports = function Product (req, res, next) {
  var slug = req.params.slug;
  var [send200, send500] = utils.getErrorHandlers(res, next);

  // @TODO make little optimization
  // menu do not need if req is xhr
  Q.all([
    models.Type.getMenu(),
    models.Product.getOne(slug)
  ])
    .spread(function (menu, product) {

      if (!product) {
          return next(ServerError404('Нет такого товара'));
      }

      var context = {
        menu: menu,
        title: product.name,
        description: product.description,
        category: product.Type.title,
        categoryUrl: product.Type.getUrl(),
        item: product.toJSON2()
      };

      if (req.xhr) {
        return send200(context);
      }

      engine.render('pages["item"]', [context, env])
        .then(send200)
        .fail(send500)
    })
    .fail(send500);
};
