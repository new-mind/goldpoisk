// vim: ts=2: sw=2:
var engine = require('express-bemhtml-priv.js');
var Q = require('q');

var models = require('../../db');
var utils = require('../../modules/utils');

var LIMIT = 5;
var env = {};

module.exports = function Home(req, res, next) {
  var [send200, send500] = utils.getErrorHandlers(res, next);

  Q.all([
    models.Type.getMenu(),
    models.BestBid.getMany(LIMIT),
    models.Banner.getPage()
  ])
    .spread(function (menu, [count, bids], promo) {

      var context = {
        menu: menu,
        promo: promo,
        products: bids,
        count: count
      };

      if (req.xhr) {
        return send200(context);
      }

      engine.render('pages["index"]', [context, env])
        .then(send200)
        .fail(send500);
    })
    .fail(send500);
};
