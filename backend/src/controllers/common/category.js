// vim: ts=2: sw=2:
var engine = require('express-bemhtml-priv.js');
var Q = require('q');

var models = require('../../db');
var utils = require('../../modules/utils');
var PaginatorFromRequest = require('../../stores/paginator').createFromRequest;

var env = {};
module.exports = function Category (req, res, next) {
  var type = req.path.match(/^\/(\w+)/)[1];
  var paginator = PaginatorFromRequest('Product', req);
  var [send200, send500] = utils.getErrorHandlers(res, next);

  models.Type.model.findOne({where: {name: type}})
    .then(function (category) {

      Q.all([
        models.Type.getMenu(type),
        models.Product.getPage(category, paginator),
        paginator.filter.load(category)
      ])
        .spread(function (menu, [count, products]) {

          var context = {
            menu: menu,
            category: category.title,
            categoryUrl: req.path,
            count: count,
            products: products,
            sortParams: paginator.sort.toJSON(),
            paginator: paginator.toJSON(type, req.path),
            filters: paginator.filter.toJSON()
          }

          if (req.xhr) {
            return send200(context);
          }

          engine.render('pages["category"]', [context, env])
            .then(send200)
            .fail(send500);
        })
        .fail(send500);
    })
    .error(send500)
};
