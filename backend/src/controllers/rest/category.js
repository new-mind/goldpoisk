// vim: ts=2: sw=2:
var Q = require('q');

var models = require('../../db');
var PaginatorFromRequest = require('../../stores/paginator').createFromRequest;

var env = {};
module.exports = function Category (req, res, next) {
  var type = req.path.match(/^\/rest\/(\w+)/)[1];
  var paginator = PaginatorFromRequest('Product', req);

  models.Type.model.findOne({where: {name: type}})
    .then(function (category) {

      models.Product.getPage(category, paginator)
        .then(function (args) {
          var [count, products] = args;

          res.json({
            count: count,
            list: products
          });
        }, function (e) {
          res.status(500).json(e);
        });

    }, function (e) {
      res.status(500).json(e);
    });
}
