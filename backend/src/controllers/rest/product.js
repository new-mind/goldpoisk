// vim: ts=2: sw=2:
var Q = require('q');
var models = require('../../db');

module.exports = function Product (req, res, next) {
  var slug = req.params.slug;

  models.Product.getOne(slug)
    .then(function (product) {
      res.json(product.toJSON2());
    })
    .fail(send500);

  function send500 (e) {
    res.sendStatus(500).json(e);
  }
}
