var Q = require('q');
var models = require('../../db');

module.exports = function Products (req, res, next) {
  var ids = req.param('ids').split('.');

  models.Product.getMany(ids)
    .then(function (products) {
      res.json({
        "count": products.length,
        "list": products
      });
    })
    .fail(send500);

  function send500(e) {
    res.sendStatus(500).json(e);
  }
};
