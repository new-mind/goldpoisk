// vim: ts=2: sw=2:
var glob = require('glob');
var _  = require('lodash');
var path = require('path');

var routes = require('../config/route');

module.exports = function initControllers (router, config) {
  // initialization controllers
  var commonCtrls = requireCtrls('src/controllers/common/*.js');
  var restCtrls = requireCtrls('src/controllers/rest/*.js');

  // initialization routing
  setCtrls(routes.COMMON_API, commonCtrls);
  setCtrls(routes.REST_API, restCtrls);

  function setCtrls (routes, ns) {
    _.forIn(routes, function (config, method) {
      method = method.toLowerCase();
      _.forIn(config, function (controllerName, route) {
        var controller = ns[controllerName];
        if (!controller) {
          throw new Error('No such controller: ' + controllerName);
        }
        router[method](route, controller);
      });
    });
  }

  function requireCtrls(where) {
    var controllers = glob.sync(path.join(config.root, where));
    var ns = {};
    controllers.forEach(function (controller) {
      var controller = require(controller);
      ns[controller.name] = controller;
    });
    return ns;
  }
}
