var initDb = require('./modules/models');
var config = require('./config');

process.env.MEDIA_DOMAIN = config.mediaDomain;
module.exports = initDb(config.db, {
    logging: console.log,
    benchmark: true
});
