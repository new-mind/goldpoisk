var express = require('express');
var config = require('./config');
var db = require('./db');

var app = express();

require('./config/express')(app, config);

db.sequelize
    .sync()
    .then(function () {
        app.listen(config.port, function () {
          console.log('Express server listening on port ' + config.port);
        });
    }).catch(function (e) {
        throw new Error(e);
    });
