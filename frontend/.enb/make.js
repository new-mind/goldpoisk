var fs = require('fs'),
    path = require('path'),
    techs = {
      // essential
      fileProvider: require('enb/techs/file-provider'),
      fileMerge: require('enb/techs/file-merge'),

      // optimization
      borschik: require('enb-borschik/techs/borschik'),

      // css
      stylus: require('enb-stylus/techs/stylus'),

      // js
      browserJs: require('enb-js/techs/browser-js'),

      // bemhtml
      bemhtml: require('enb-bemxjst/techs/bemhtml'),
      bemjsonToHtml: require('enb-bemxjst/techs/bemjson-to-html'),

      // privJs
      privJs: require('./techs/priv.js'),
    },
    enbBemTechs = require('enb-bem-techs'),
    merged = require('./techs/merged');

var levels = [
  { path: 'libs/bem-core/common.blocks', check: false },
  { path: 'libs/bem-core/desktop.blocks', check: false },
  { path: 'libs/bem-history/common.blocks', check: false },
  { path: 'libs/bem-social/desktop.blocks', check: false },
  { path: 'libs/bem-social/design/common.blocks', check: false },
  { path: 'libs/bem-social/desktop.blocks', check: false },
  'bem-goldpoisk/desktop.blocks',
  'common.blocks',
  'desktop.blocks',
  'min.blocks'
];

if (process.env.NODE_ENV === 'production') {
  levels.push('production.blocks');
}

module.exports = function(config) {
  var isProd = process.env.NODE_ENV === 'production',
      mergedBundleName = 'merged',
      pathToMargedBundle = path.join('desktop.bundles', mergedBundleName);

  fs.existsSync(pathToMargedBundle) || fs.mkdirSync(pathToMargedBundle);

  merged(config, pathToMargedBundle);

  config.nodes('*.bundles/*', function(nodeConfig) {
    var isMergedNode = path.basename(nodeConfig.getPath()) === mergedBundleName;

    isMergedNode || nodeConfig.addTechs([
      [techs.fileProvider, { target: '?.bemjson.js' }],
      [enbBemTechs.bemjsonToBemdecl]
    ]);

    nodeConfig.addTechs([
      // essential
      [enbBemTechs.levels, { levels: levels }],
      [enbBemTechs.deps],
      [enbBemTechs.files],

      // css
      [techs.stylus, {
        target: '?.css',
        autoprefixer: {
          browsers: ['ie >= 10', 'last 2 versions', 'opera 12.1', '> 2%']
        }
      }],

      // bemhtml
      [techs.bemhtml, { devMode: process.env.BEMHTML_ENV === 'development' }],

      // html
      [techs.bemjsonToHtml],

      // client bemhtml
      [enbBemTechs.depsByTechToBemdecl, {
        target: '?.bemhtml.bemdecl.js',
        sourceTech: 'js',
        destTech: 'bemhtml'
      }],
      [enbBemTechs.deps, {
        target: '?.bemhtml.deps.js',
        bemdeclFile: '?.bemhtml.bemdecl.js'
      }],
      [enbBemTechs.files, {
        depsFile: '?.bemhtml.deps.js',
        filesTarget: '?.bemhtml.files',
        dirsTarget: '?.bemhtml.dirs'
      }],
      [techs.bemhtml, {
        target: '?.browser.bemhtml.js',
        filesTarget: '?.bemhtml.files',
        devMode: process.env.BEMHTML_ENV === 'development'
      }],

      // js
      [techs.browserJs, { includeYM: true }],
      [techs.fileMerge, {
        target: '?.js',
        sources: ['?.browser.js', '?.browser.bemhtml.js']
      }],

      // privJs
      [techs.privJs, {
        target: '?.priv.js',
        filesTarget: '?.files',
        sourceSuffixes: ['priv.js'],
        sourcemap: true
      }],

      // borschik
      [techs.borschik, { source: '?.js', target: '?.min.js', freeze: true, minify: isProd }],
      [techs.borschik, { source: '?.css', target: '?.min.css', tech: 'cleancss', freeze: true, minify: isProd }],
      [techs.borschik, { source: '?.bemhtml.js', destTarget: '?.bemhtml.min.js', minify: isProd }],
      [techs.borschik, { source: '?.priv.js', destTarget: '?.priv.min.js', minify: isProd }]
    ]);

    nodeConfig.addTargets(['?.bemhtml.min.js', '?.priv.min.js', '?.min.css', '?.min.js']);
    isMergedNode || nodeConfig.addTargets(['?.html']);
  });
};
