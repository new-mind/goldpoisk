modules.define('config', ['i-bem'], function (provide) {
    provide({
        logLevel: 'debug',
        scripts: {
            slider: 'http://goldpoisk.ru/js/third-parties/jssor.slider.min.js',
            lodash: 'http://goldpoisk.ru/js/third-parties/lodash.min.js'
        },
        REST: {
            rings: {
                list: 'http://localhost:3000/success'
            },
            searchUrl: '/search',
            notFoundUrl: '/not-found',
            products: '/rest/products'
        },
        page: {
            index: {
                url: '^/$',
                priv: 'index.content'
            },
            necklaces: {
                url: '^/necklaces$',
                priv: 'category.content'
            },
            chains: {
                url: '^/chains$',
                priv: 'category.content'
            },
            pendants: {
                url: '^/pendants$',
                priv: 'category.content'
            },
            bracelets: {
                url: '^/bracelets$',
                priv: 'category.content'
            },
            rings: {
                url: '^/rings$',
                priv: 'category.content'
            },
            earrings: {
                url: '^/earrings$',
                priv: 'category.content'
            },
            brooches: {
                url: '^/brooches$',
                priv: 'category.content'
            },
            watches: {
                url: '^/watches$',
                priv: 'category.content'
            },
            cutlery: {
                url: '^/cutlery$',
                priv: 'category.content'
            },
            necklacesItem: {
                url: '^/necklaces/[\\w\\d-]+$',
                priv: 'item.content'
            },
            chainsItem: {
                url: '^/chains/[\\w\\d-]+$',
                priv: 'item.content'
            },
            pendantsItem: {
                url: '^/pendants/[\\w\\d-]+$',
                priv: 'item.content'
            },
            braceletsItem: {
                url: '^/bracelets/[\\w\\d-]+$',
                priv: 'item.content'
            },
            ringsItem: {
                url: '^/rings/[\\w\\d-]+$',
                priv: 'item.content'
            },
            earringsItem: {
                url: '^/earrings/[\\w\\d-]+$',
                priv: 'item.content'
            },
            broochesItem: {
                url: '^/brooches/[\\w\\d-]+$',
                priv: 'item.content'
            },
            watchesItem: {
                url: '^/watches/[\\w\\d-]+$',
                priv: 'item.content'
            },
            cutleryItem: {
                url: '^/cutlery/[\\w\\d-]+$',
                priv: 'item.content'
            },
            notfound: {
                url: '^/not-found$',
                priv: 'notfound.content'
            },
            best: {
                url: '^/best$',
                priv: 'category.content'
            }
        }
    })
});
