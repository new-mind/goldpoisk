var blocks = {};
var pages = {};
var HOST = 'http://goldpoisk.ru';

/**
 *  @param {Object} data
 *      @key {String} title
 *      @key {String} description
 *      @key {String} type
 *      @key {String} image
 *      @key {String} url
 *  @TODO @key {Object} openGraph
 *          @key {String} type // Тип страницы товар|магазин|каталог
 *          @key {String} title // og заголовок
 *          @key {String} description // og описание
 *          @key {String} image // og изображение
 *          @key {String} url // og ссылка
 **/
blocks['page'] = function (data, env) {
    var menu = data.menu && blocks['g-menu'](data.menu, env) || {};
    return {
        block: 'page',
        title: data.title,
        favicon: '/images/favicon.png',
        head: [
            { elem: 'meta', attrs: { name: 'yandex-verification', content: '7c8bcf3b65216908' }},
            { elem: 'meta', attrs: { name: 'description', content: data.description } },
            { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
            { elem: 'css', url:  '/css/index.css' },
            data.type == 'product' ? ([
                { elem: 'meta', attrs: { property: 'og:type', content: data.type } },
                { elem: 'meta', attrs: { property: 'og:title', content: data.title } },
                { elem: 'meta', attrs: { property: 'og:description', content: data.description } },
                { elem: 'meta', attrs: { property: 'og:image', content: data.image } },
                { elem: 'meta', attrs: { property: 'og:url', content: data.url } }
            ]) : null
        ],
        scripts: [
            { elem: 'js', url: env.production ? '//yastatic.net/jquery/2.1.3/jquery.min.js' : '/js/third-parties/jquery.min.js' },
            { elem: 'js', url: '/js/third-parties/jssor.slider.min.js' },
            { elem: 'js', url: '/js/third-parties/lodash.min.js' },
            { elem: 'js', url: '//yastatic.net/es5-shims/0.0.2/es5-shims.min.js' },
            { elem: 'js', url: '//yastatic.net/share2/share.js' },
            { elem: 'js', url: '/js/index.bemhtml.js' },
            { elem: 'js', url: '/js/index.priv.js' },
            { elem: 'js', url: '/js/index.js' }
        ],
        mods: { theme: 'normal' },
        content: [
            blocks['g-header'](null, env),
            menu,
            data.content,

            {
                block: 'g-footer',
                content: []
            }
        ]
    }
}

pages['index'] = function (data, env) {
    return blocks['page']({
        title: 'Самый большой каталог ювелирных изделий',
        description: 'Goldpoisk - это все ювелирные изделия от лучших производителей на одном сайте. Огромный выбор из более 20 000 товаров. Удобный, бесплатный поиск и фильтр по подбору нужного ювелирного изделия.',
        menu: data.menu,
        content: [
            {
                block: 'g-content',
                content: [
                    blocks['g-promotion'](data.promo, env),
                    {
                        block: 'g-pride',
                        content: [
                            {
                                block: 'g-pride-item',
                                mods: { type: 'brown' },
                                label: 'Лучшие производители ювелирных изделий'
                            },
                            {
                                block: 'g-pride-item',
                                mods: { type: 'beige' },
                                label: 'Огромный ассортимент представлен в каталоге'
                            },
                            {
                                block: 'g-pride-item',
                                mods: { type: 'gold' },
                                label: 'Только проверенные партнёры и предложения'
                            },
                            {
                                block: 'g-pride-item',
                                mods: { type: 'gray' },
                                label: 'Простой и удобный поиск по параметрам'
                            }
                        ]
                    }, {
                        block: 'g-section',
                        title: 'Goldpoisk - самый большой каталог ювелирных изделий',
                        description: 'Все ювелирные изделия от лучших производителей собраны здесь.<br>Огромный выбор из 150000 товаров.'
                    },
                    blocks['g-bids']({
                        count: data.count || 0, //TODO:
                        products: data.products,
                        url: '/best'
                    }),
                    {
                        block: 'g-cooperation',
                        content: []
                    }
                ]
            }
        ]
    }, env)
}

pages['index.str'] = function (data, env) {
    return pages['index'](JSON.parse(data), env);
}

pages['index.content'] = function (data, env) {
    return [
        blocks['g-promotion'](data.promo, env),
        {
            block: 'g-pride',
            content: [
                {
                    block: 'g-pride-item',
                    mods: { type: 'brown' },
                    label: 'Лучшие производители ювелирных изделий'
                },
                {
                    block: 'g-pride-item',
                    mods: { type: 'beige' },
                    label: 'Огромный ассортимент представлен в каталоге'
                },
                {
                    block: 'g-pride-item',
                    mods: { type: 'gold' },
                    label: 'Только проверенные партнёры и предложения'
                },
                {
                    block: 'g-pride-item',
                    mods: { type: 'gray' },
                    label: 'Простой и удобный поиск по параметрам'
                }
            ]
        }, {
            block: 'g-section',
            title: 'Goldpoisk - самый большой каталог ювелирных изделий',
            description: 'Все ювелирные изделия от лучших производителей собраны здесь.<br>Огромный выбор из 150000 товаров.'
        },
        blocks['g-bids']({
            count: data.count || 0, //TODO:
            products: data.products,
            url: '/best'
        }),
        {
            block: 'g-cooperation',
            content: []
        }
    ]
}

pages['notfound.content'] = function (data, env) {
    return blocks['g-bids']({
        count: data.count || 0, //TODO:
        products: data.products,
        url: '#'
    }, {
        error404: true
    });
}

/**
 * data.menu
 * {String} !data.products
 */
pages['category'] = function (data, env) {
    env = env || {};
    env.headerJs = true;

    return blocks['page']({
        title: data.title,
        description: data.description,
        menu: data.menu,
        content: {
            block: 'g-content',
            content: pages['category.content'](data, env)
        }
    }, env);
}

pages['category.json'] = function (data, env) {
    return pages['category'](JSON.parse(data), env)
}

pages['category.content'] = function (data, env) {
    return [
        blocks['g-breadcrumbs'](data.category, data.categoryUrl),
        {
            block: 'g-category-title',
            title: data.category,
            count: data.count
        },
        blocks['g-goods']({
            count: data.count,
            list: data.products,
            sortParams: data.sortParams,
            filters: data.filters
        }, {showFrame: true}),
        blocks['g-paginator'](data.paginator)
    ]
}

/**
 * data.menu
 * data.item
 *
 * {String} data.category
 * {String} data.categoryUrl
 */
pages['item'] = function (data, env) {
    env = env || {};
    env.page = 'item';

    return blocks['page']({
        title: data.title,
        description: data.description,
        image: data.item.images[0],
        url: HOST + data.item.url,
        type: 'product',
        menu: data.menu,
        content: {
            block: 'g-content',
            content: [
                blocks['g-breadcrumbs'](data.category, data.categoryUrl, data.title),
                blocks['g-item'](data.item, env)
            ]
        }
    }, env)
}

pages['item.content'] = function (data, env) {
    env = env || {};
    env.page = 'item';

    return [
        blocks['g-breadcrumbs'](data.category, data.categoryUrl, data.title),
        blocks['g-item'](data.item, env)
    ]
}

//TODO:
pages['item.json'] = function (data, env) {
    return blocks['page']({
        title: data.title,
        description: data.description,
        menu: data.menu,
        content: {
            block: 'g-content',
            content: [
                blocks['g-breadcrumbs'](data.category, data.categoryUrl, data.title),
                {
                    block: 'g-item',
                    content: [
                        blocks['g-item'](JSON.parse(data.item), {big: true})
                    ]
                }
            ]
        }
    }, env)
}
/**
 *  @param {Object} data
 *      @key {String} title
 *      @key {String} description
 *      @key {Number} code
 *      @key {Number} count
 *      @key {Array} products
 **/
pages['error'] = function (data, env) {
    return blocks['page']({
        title: data.title,
        description: data.description,
        content: {
            block: 'g-content',
            content: [
                blocks['g-error']({
                    code: data.code,
                    description: data.description
                }),
                blocks['g-bids']({
                    count: data.count || 0, //TODO:
                    products: data.products,
                    url: '#'
                })
            ]
        }
    }, env);
};

/**
 *  @param {Object} data
 *      @key {String} title
 *      @key {String} description
 *      @key {Object[]} menu
 *      @key {Object[]} articles
 *          @key {String} article.image
 *          @key {String} article.title
 *          @key {String} article.url
 *          @key {String} article.date
 *          @key {String} article.text
 *          @key {Boolean} article.isCorporate
 **/
pages['articles'] = function (data, env) {
    return blocks['page']({
        titlle: data.title,
        description: data.description,
        menu: data.menu,
        content: {
            block: 'g-content',
            content: [
                blocks['g-articles']({
                    articles: data.articles
                }),
                {
                    block: 'g-cooperation',
                    content: []
                }
            ]
        }
    }, env);
};

/**
 *  @param {Object} data
 *      @key {String} title
 *      @key {String} description
 *      @key {Object[]} articles
 *          @key {String} article.image
 *          @key {String} article.title
 *          @key {String} article.url
 *          @key {String} article.date
 *          @key {String} article.text
 *          @key {Boolean} article.isCorporate
 **/
pages['articles.content'] = function (data, env) {
    return [
        blocks['g-articles']({
            articles: data.articles
        }),
        {
            block: 'g-cooperation',
            content: []
        }
    ];
};

function assertHas (obj, key, message) {
    if (!obj[key])
        throw new Error(message);
}

function declension(count, word) {
    /**
     *  @param {Integer} count
     *  @param {String} word
     *  @return {String} result = count + word
     **/
    count = count.toString();
    var basis = word.substr(0, word.length - 2);
    var ending = word.substr(-2);
    var lastLetters = count.substr(-2);
    if (ending == 'ие') {
        if (["11", "12", "13", "14"].indexOf(lastLetters) != -1) {
            return count + " " + basis + 'ий';
        }
        switch (lastLetters.substr(-1)) {
            case '1':
                return count + " " + basis + ending;
                break;
            case '2':
            case '3':
            case '4':
                return count + " " + basis + 'ия';
                break;
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '0':
                return count + " " + basis + 'ий';
                break;
            default:
                break;
        }
    } else if (ending == 'ар') {
        if (['11', '12', '13', '14'].indexOf(lastLetters) != -1) {
            return basis + ending + 'ов';
        }
        switch (lastLetters.substr(-1)) {
            case '1':
                return basis + ending;
                break;
            case '2':
            case '3':
            case '4':
                return basis + ending + 'а';
                break;
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '0':
                return basis + ending + 'ов'
        }
    } else {
        return 'Oops';
    }
}

function capitalize (word) {
    word = word || "";
    word = word.toLowerCase();
    word = word.charAt(0).toUpperCase() + word.substr(1);
    return word;
}

/**
 *  @param {String} date - 2016-02-14
 *  @return {String} formatDate - 2 февраля
 **/
function formatDate (date) {
    var date = new Date(date);
    var currentYear = new Date().getFullYear();
    var MONTHS = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

    return date.getDate() + ' ' + MONTHS[date.getMonth()] + (currentYear != date.getFullYear() && date.getFullYear());
}
