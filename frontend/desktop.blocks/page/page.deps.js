({
    shouldDeps: [{
        block: 'metrika'
    }, {
        block: 'google_analytics'
    }, {
        block: 'i-bem',
        elem: 'dom',
        mods: { 'init': 'auto' }
    }]
})
