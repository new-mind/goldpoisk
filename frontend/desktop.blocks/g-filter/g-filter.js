modules.define('g-filter',
    ['i-bem__dom', 'jquery', 'logger', 'router', 'cookie', 'i-scroll', 'events__channels'],
    function(provide, BEMDOM, $, logger, router, cookie, scroll, channels) {
    BEMDOM.decl('g-filter', {
        onSetMod: {
            js: {
                'inited': function () {
                    this._filters = {};
                    var self = this;
                    var page = this.findBlockOutside('page');

                    this._logger = logger.Logger('g-filter').init();
                    this._data = {};
                    this._blocks = {
                        scrollButton: self.findBlockInside('g-filter__scroll'),
                        header: page.findBlockInside('g-header'),
                        footer: page.findBlockInside('g-footer'),
                        goods: page.findBlockInside('g-goods')
                    };

                    cookie.get('filter_hidden') === 'true' ? this.setMod('hidden') : this.delMod('hidden');

                    this.bindTo(this.elem('button'), 'click', function (e) {
                        this.toggleMod('hidden');
                    });

                    this._bindScroll();

                    BEMDOM.blocks['g-filter-param'].on(
                        this.domElem,
                        'init',
                        this._onInitFn,
                        this
                    );

                    BEMDOM.blocks['g-filter-param'].on(
                        this.domElem,
                        'changed',
                        this._onChangeFn,
                        this
                    );

                    this._moveFn = function (e) {
                        var isHidden = self.hasMod('hidden');
                        var startPosition = self._blocks.goods.elem('aside').offset().top;
                        var windowScrollTop = $(window).scrollTop();

                        var filterHeight = self.domElem.outerHeight();
                        var stickyHeaderHeight = self._blocks.header.findBlockInside('g-header-inner').domElem.outerHeight();

                        var goTo = windowScrollTop - startPosition + stickyHeaderHeight;

                        self.setMod('hidden', 'force');

                        if (windowScrollTop + stickyHeaderHeight + filterHeight > self._blocks.footer.domElem.offset().top) {
                            goTo = self._blocks.footer.domElem.offset().top - startPosition - filterHeight;
                        }

                        self.domElem.css('top', goTo + 'px');

                        if (isHidden) {
                            self.setMod('hidden', true);
                        }
                        self.delMod('hidden');
                        this.setMod('hidden');
                    };
                    this._blocks.scrollButton.bindTo('click', this._moveFn);
                },
                '': function () {
                    this.unbindFrom(this.elem('button'), 'click');
                    this.unbindFromWin('scroll', this._scrollFn);
                    this._unbindScroll();

                    this._logger.finalize();
                    this._logger = null;
                    this._data = null;
                    this._blocks = null;
                }
            },
            'hidden': {
                true: function () {
                    channels('filter').emit('changestate', true);
                    this._blocks.scrollButton.setMod('narrow');
                    cookie.set('filter_hidden', true, {
                        expires: 1
                    });
                },
                '': function () {
                    channels('filter').emit('changestate', false);
                    this._blocks.scrollButton.delMod('narrow');
                    cookie.set('filter_hidden', false, {
                        expires: 1
                    });
                }
            }
        },

        _onChangeFn: function (e, data) {
            var name = data.type;
            var value = data.ids;

            this._filters[name] = value;
            channels('filter').emit('change', { filters: this._filters });
        },

        _onInitFn: function (e, data) {
            var name = data.type;
            var value = data.ids;
            this._filters[name] = value;
            channels('filter').emit('init', { filters: this._filters });
        },

        _bindScroll: function () {
            var self = this;
            var scrollButton = self._blocks.scrollButton;

            this._scrollTopFn = function (e) {
                var windowScrollTop = $(window).scrollTop();
                var filterTop = self.domElem.offset().top;

                var filterHeight = self.domElem.outerHeight();
                var windowHeight = $(window).outerHeight();
                var stickyHeaderHeight = self._blocks.header.findBlockInside('g-header-inner').domElem.outerHeight();
                var startPosition = self._blocks.goods.elem('aside').offset().top;

                // Если скрол ниже шапки
                if (windowScrollTop + stickyHeaderHeight <= filterTop) {
                    var val =  windowScrollTop - startPosition + stickyHeaderHeight;
                    val = val > 0 ? val : 0;
                    self.domElem.css('top', val + 'px');
                }

                // Если скрол ниже фильтра на два экрана
                if ((windowScrollTop - (filterTop + filterHeight) > 2 * windowHeight)) {
                    scrollButton.delMod('hidden');
                } else {
                    scrollButton.setMod('hidden');
                }
            };

            this._scrollBottomFn = function (e) {
                var windowScrollTop = $(window).scrollTop();
                var filterTop = self.domElem.offset().top;

                var filterHeight = self.domElem.outerHeight();
                var windowHeight = $(window).outerHeight();
                var stickyHeaderHeight = self._blocks.header.findBlockInside('g-header-inner').domElem.outerHeight();

                // Если скрол ниже фильтра на два экрана
                if ((windowScrollTop - (filterTop + filterHeight) > 2 * windowHeight)) {
                    scrollButton.delMod('hidden');
                } else {
                    scrollButton.setMod('hidden');
                }
            };

            scroll.on('scrollTop', this._scrollTopFn);
            scroll.on('scrollBottom', this._scrollBottomFn);
        },

        _unbindScroll: function () {
            scroll.un('scrollTop', this._scrollTopFn);
            scroll.un('scrollBottom', this._scrollBottomFn);
        },

        _pending: false,
        _data: null,
        _blocks: null,
        _logger: null,

        _moveFn: null,
        _filters: null,
        _scrollTopFn: null,
        _scrollBottomFn: null

    }, {});
    provide(BEMDOM);
});
