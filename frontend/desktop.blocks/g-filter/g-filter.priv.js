/**
 *  @param {Object} data
 *      @hidden {Boolean} [hidden]
 *      @key {Object[]} params
 *          @key {String} param.title
 *          @key {String} param.type
 *          @key {String} [param.state], 'opened'|'closed', default='opened'
 *          @key {Object} param.value
 *              @option {Object[]} values
 *                  @key {Number} value.id
 *                  @key {String} value.name
 *              @option {Object} price
 *                  @key {Number} min
 *                  @key {Number} max
 *                  @key {Number} [scale] - scale of range (range width), default=162
 *                  @key {Number} [ticks] - count of separate units, default=2
 *                  @key {Number} [step] - step size, default=1000
 *                  @key {Number} [diff] - min difference between min and max values, default=5000
 **/
blocks['g-filter'] = function (data, env) {
    assertHas(data, 'params', 'Should point params');
    var params = [];

    data.params.map(function (param) {
        params.push(blocks['g-filter-param'](param, env));
    });

    return {
        block: 'g-filter',
        mods: { 'hidden': !!data.hidden },
        tag: 'section',
        content: [{
            elem: 'title',
            tag: 'h2',
            content: 'Поиск по параметрам'
        }, {
            elem: 'list',
            content: params
        }, {
            elem: 'button'
        }]
    };
};
