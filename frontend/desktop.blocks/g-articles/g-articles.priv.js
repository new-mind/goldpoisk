/**
 *  @param {Object} data
 *      @key {Object[]} articles
 *          @key {String} article.image
 *          @key {String} article.title
 *          @key {String} article.url
 *          @key {String} article.date
 *          @key {String} article.text
 *          @key {Boolean} article.isCorporate
 **/
blocks['g-articles'] = function (data, env) {
    return {
        block: 'g-articles',
        content: data.articles.map(function (article, i) {
            return {
                block: 'g-article-item',
                mods: {
                    size: (i == 0 || (i % 6 == 0)) ? 'l' : 's'
                },
                content: article
            };
        })
    };
};
