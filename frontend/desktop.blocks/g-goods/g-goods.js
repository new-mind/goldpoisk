modules.define('g-goods',
    ['i-bem__dom', 'logger', 'router', 'keyboard__codes', 'events__channels', 'cookie'],
    function(provide, BEMDOM, logger, router, key, channels, cookie) {
    var FILTR_DELAY = 1500;
    var ITEMS_ON_PAGE = 30;

    BEMDOM.decl('g-goods', {
        onSetMod: {
            js: {
                'inited': function () {
                    this._cash = {};
                    this._logger = logger.Logger('g-goods').init();
                    var self = this;
                    this._pending = false;
                    var currentProduct = null;

                    this._totalCount = this.params.count;
                    this._selected = null;
                    this._products = this.params.products;

                    this.on('select', _.callback(this._selectProduct, this));
                    this.on('unselect', _.callback(this._unselectProduct, this));

                    cookie.get('filter_hidden') === 'true' ? this.setMod('wide') : this.delMod('wide');

                    this._controlKeyFn = function (e) {
                        if (e.which == key.LEFT) {
                            var prev = self._getPrevProduct(self._selected);
                            if (!prev)
                                return;
                            prev.select();
                        } else if (e.which == key.RIGHT) {
                            var next = self._getNextProduct(self._selected);
                            if (!next)
                                return;
                            next.select();
                        }
                    };

                    channels('filter').on('init', this._onFilterInitFn, this);
                    channels('filter').on('change', this._onFilterFn, this);
                    channels('filter').on('changestate', this._onFilterChangeStateFn, this);

                    var sorting = this.findBlockOutside('g-content').findBlockInside('g-sorting-goods');
                    if (sorting)
                        sorting.on('sort', function (e, value) {
                            self.loading(true);


                            router.setParams({sort: value});
                            channels('goods').emit('loadItems', { page: 1 });

                            var uri = router.getUri('/rest' + router.getPath());
                            var url = uri.toString();

                            if (self._products.length == self._totalCount) {
                                var sortFunc = {
                                    name: function (a, b) {
                                        if (a.title > b.title)
                                            return 1;
                                        return -1;
                                    },
                                    price: function (a, b) {
                                        if (a.minPrice > b.minPrice)
                                            return 1;
                                        return -1;
                                    },
                                    tprice: function (a, b) {
                                        if (a.minPrice > b.minPrice)
                                            return -1;
                                        return 1;
                                    }
                                }
                                self._products.sort(sortFunc[value]);
                                var data = {
                                    count: self._products.length,
                                    list: self._products
                                };
                                self.update(data);
                                self.loading(false);
                                return;
                            }
                            $.getJSON(url, function sort (data) {
                                self.update(data);
                                self.loading(false);
                            })
                        });

                    this._logger = logger.Logger('g-goods');
                },

                '': function () {
                    this._logger.finalize();
                    this._logger = null;
                    this.un('select');
                    this.un('unselect');
                    channels('filter').un('inited', this._onFilterInitFn, this);
                    channels('filter').un('change', this._onFilterFn, this);
                    channels('filter').un('changestate', this._onFilterChangeStateFn, this);
                }
            },

            //TODO:
            'loading': {
                true: function () {
                    console.log('loading start');
                },
                '': function () {
                    console.log('loading end');
                }
            }
        },

        _onFilterInitFn: function (e, data) {
            this._filters = this._convertFilters(data.filters);
        },

        _onFilterChangeStateFn: function (e, isHide) {
            isHide ? this.setMod('wide') : this.delMod('wide');
        },

        _convertFilters: function (filters) {
            return _.mapValues(filters, function (value, key) {
                if (_.isArray(value) && !_.isEmpty(value))
                    return value.join('.');

                return _.isNumber(value) ? value : null;
            });
        },

        _onFilterFn: _.debounce(function (e, data) {
            var filters = this._convertFilters(data.filters);

            if (_.isEqual(this._filters, filters))
                return;
            this._filters = _.clone(filters);

            if (this._pending)
                return;

            this._logger.debug('Filtering', JSON.stringify(this._filters));

            var self = this;

            this._pending = true;
            this.loading(true);
            channels('goods').emit('beforeload', { page: 1 });
            router.setParams(this._filters);
            var uri = router.getUri('/rest' + router.getPath());
            var url = uri.toString();

            this._logger.debug('Requesting', url);
            $.getJSON(url, function (data) {
                self._pending = false;
                self.update(data);
                self.loading(false);
                channels('goods').emit('load', { totalPages: Math.ceil(data.count / ITEMS_ON_PAGE) });
            });
            $("body,html").animate({scrollTop: 0}, 750, 'easeInQuart');
        }, FILTR_DELAY),

        loading: function (isLoading) {
            var sort = this.findBlockOutside('g-content').findBlockInside('g-sorting-goods');
            if (!!isLoading) {
                this.setMod(this.elem('content'), 'loading', true);
                sort.setMod('loading', true);
            } else {
                this.setMod(this.elem('content'), 'loading', false);
                sort.setMod('loading', false);
            }
        },

        /**
         * Method for appending g-products to the end of container
         * @param {Array} list
         */
        append: function (list) {
            if (!_.isArray(list))
                this._logger.throw('Should point {Array} list');

            this._products = this._products.concat(list);

            var bemjson = blocks['g-goods.items'](list, {showFrame: true});

            BEMDOM.append(
                this.elem('content'),
                BEMHTML.apply(bemjson)
            );
        },

        /**
         * Method for prepending g-products to the end of container
         * @param {Array} list
         */
        prepend: function (list) {
            if (!_.isArray(list))
                this._logger.throw('Should point {Array} list');

            this._products = list.concat(this._products);

            var bemjson = blocks['g-goods.items'](list, {showFrame: true});

            BEMDOM.prepend(
                this.elem('content'),
                BEMHTML.apply(bemjson)
            );
        },

        /**
         * There is method self replaces own content with new data
         * @param {Object} data
         *   @key {Array} list
         *   @key {Number} count
         */
        update: function (data) {
            if (!_.isObject(data))
                this._logger.throw('Should point {Object} data');

            var count = this._totalCount = data.count;
            var list = this._products = data.list;

            if (!_.isNumber(count))
                this._logger.throw('Should point {Number} count');

            if (!_.isArray(list))
                this._logger.throw('Should point {Array} list');

            var bemjson = blocks['g-goods.items'](list, {showFrame: true});
            //TODO: govnokot
            this.findBlockOutside('g-content')
                .findBlockInside('g-category-title')
                .elem('count').text('(' + count + ')');

            BEMDOM.update(
                this.elem('content'),
                BEMHTML.apply(bemjson)
            );
        },

        _getPrevProduct: function (product) {
            var currentIndex = this._getCurrentIndex(this._selected);
            if (!currentIndex)
                return null;
            var prevIndex = currentIndex - 1;
            return ~prevIndex ? this.findBlocksInside('g-product')[prevIndex] : null;
        },

        _getNextProduct: function (product) {
            var currentIndex = this._getCurrentIndex(this._selected);
            if (currentIndex === this._products.length - 1)
                return null;
            var nextIndex = currentIndex + 1;
            return ~nextIndex ? this.findBlocksInside('g-product')[nextIndex] : null;
        },

        _getCurrentIndex: function (id) {
            return _.findIndex(this._products, function (product) {
                return id == product.id;
            });
        },

        _getProduct: function (id) {
            var index = this._getCurrentIndex(id);
            return this.findBlocksInside('g-product')[index];
        },

        _selectProduct: function(e, product) {
            var id = product.params.id;
            // unselect old product
            if (this._selected && this._selected !== id) {
                this._getProduct(this._selected).unselect();
            }
            this._selected = id;
            this.bindToDoc('keyup', this._controlKeyFn);
        },

        _unselectProduct: function (e) {
            this.unbindFromDoc('keyup', this._controlKeyFn);
        },

        _products: [],
        _totalCount: null,
        _selected: null
    }, {});
    provide(BEMDOM);
})
