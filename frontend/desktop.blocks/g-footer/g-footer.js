modules.define('g-footer', ['i-bem__dom', 'router'], function(provide, BEMDOM, router) {
    BEMDOM.decl('g-footer', {
        onSetMod: {
           'js': function () {
                this.findBlockInside('g-link').bindTo('click', function (e) {
                    e.preventDefault();
                    router.route('/');
                })
           }
        }
    }, {});
    provide(BEMDOM);
});