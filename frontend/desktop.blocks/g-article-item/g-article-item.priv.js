/**
 *  @param {Object} data
 *      @key {String} size
 *      @key {String} image
 *      @key {String} title
 *      @key {String} url
 *      @key {String} date
 *      @key {String} text
 *      @key {Boolean} isCorporate
 **/
blocks['g-article-item'] = function (data, env) {
    return {
        block: 'g-article-item',
        mods: {
            size: data.size,
            сorporate: data.isCorporate
        },
        content: {
            image: data.image,
            title: data.title,
            url: data.url,
            date: formatDate(data.date),
            content: data.text
        }
    }
};
