modules.define('g-breadcrumbs', ['i-bem__dom', 'router'], function (provide, BEMDOM, router) {

    BEMDOM.decl('g-breadcrumbs', {
        onSetMod: {
            js: {
                'inited': function () {
                    this.bindTo(this.elem('root-node'), 'click', function (e) {
                        e.preventDefault();
                        var url = $(e.target).attr('href');
                        router.route(url);
                    });

                    this.bindTo(this.elem('top-level-node'), 'click', function (e) {
                        e.preventDefault();
                        var url = $(e.target).attr('href');
                        router.route(url);
                    });
                }
            },
            '': function () {
                this.unbindFrom(this.elem('root-node'), 'click');
                this.unbindFrom(this.elem('top-level-node'), 'click');
            }
        }
    }, {});

    provide(BEMDOM);
});
