modules.define('g-filter-param',
    ['i-bem__dom', 'jquery', 'logger', 'router', 'cookie'],
    function (provide, BEMDOM, $, logger, router, cookie) {

    BEMDOM.decl('g-filter-param', {
        onSetMod: {
            js: {
                'inited': function () {
                    this._params = [];
                    this._logger = logger.Logger('g-filter-param #' + this.params.type).init();
                    this.setMod('state', cookie.get(this._getCookieName()) || 'opened');
                    var checkboxes = this.findBlocksInside('g-checkbox');
                    var self = this;
                    var enabled = router.getParam(this.params.type);
                    if (enabled) {
                        _.forEach(enabled.split('.'), function (id) {
                            id = parseInt(id, 10);
                            var checkbox = _.find(checkboxes, function (item) {
                                return item.params.ident === id
                            });

                            if (!checkbox) {
                                self._logger.debug('There is no item with id', id);
                                return;
                            }

                            checkbox.setMod('checked');
                            self._params.push(id);
                            self.emit('init', {
                                type: self.params.type,
                                ids: self._params
                            });
                        });
                    }

                    this._bind();
                },
                '': function () {
                    this.unbindFrom(this.elem('title_arrow'), 'click');
                    this.unbindFrom(this.elem('title_text'), 'click');
                    BEMDOM.blocks['g-checkbox'].un(
                        this.domElem,
                        {
                            modName: 'checked',
                            modVal: '*'
                        },
                        this._onCheckFn,
                        this
                    );

                    this._logger.finalize();
                    this._logger = null;
                }
            }
        },

        _getCookieName: function () {
            return this.params.type + '_state';
        },

        _bind: function () {
            var self = this;

            this.bindTo(this.elem('title_arrow'), 'click', function(e) {
                this._onParamClick(e);
            });

            this.bindTo(this.elem('title_text'), 'click', function(e) {
                this._onParamClick(e);
            });

            BEMDOM.blocks['g-checkbox'].on(
                this.domElem,
                {
                    modName: 'checked',
                    modVal: '*'
                },
                this._onCheckFn,
                this
            );

            BEMDOM.blocks['g-price-control'].on(
                this.domElem,
                'changeMinPrice',
                function (e, data) {
                    self.emit('changed', data);
                },
                this
            );

            BEMDOM.blocks['g-price-control'].on(
                this.domElem,
                'changeMaxPrice',
                function (e, data) {
                    self.emit('changed', data);
                },
                this
            );
        },

        _onCheckFn: _.debounce(function (e, checkbox) {
            var target = e.target;
            var id = target.params.ident;

            !!checkbox.modVal ?
                this._addParam(id) :
                this._delParam(id);
        }, 100),

        _addParam: function (id) {
            this._logger.debug('Add param', id);

            if (_.find(this._params, function (i) { return i == id; }))
                return;

            this._params.push(id);
            this.emit('changed', {
                type: this.params.type,
                ids: this._params
            });
        },

        _delParam: function (id) {
            this._logger.debug('Remove param', id);
            var index = this._params.indexOf(id);
            if (!~index)
                return;

            var params;
            if (this._params.length) {
                params = this._params;
            } else {
                params = null;
            }

            this._params.splice(index, 1);
            this.emit('changed', {
                type: this.params.type,
                ids: params
            });
        },

        _onParamClick: function (e) {
            this.toggleMod('state', 'opened', 'closed');

            if (this.hasMod('state', 'opened'))
                cookie.set(this.params.type + '_state', 'opened');
            else
                cookie.set(this.params.type + '_state', 'closed');
        },

        _params: []
    }, {});
    provide(BEMDOM);
});
