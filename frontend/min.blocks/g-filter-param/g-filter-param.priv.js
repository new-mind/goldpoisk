/**
 *  @param {Object} data
 *      @key {String} title
 *      @key {String} type
 *      @key {String} state, 'opened'|'closed', default='opened'
 *      @key {Object[]} value
 *          @option {Object[]} values
 *              @key {Number} value.id
 *              @key {String} value.name
 *          @option {Object} price
 *              @key {Number} min
 *              @key {Number} max
 *              @key {Number} [scale] - scale of range (range width), default=162
 *              @key {Number} [ticks] - count of separate units, default=2
 *              @key {Number} [step] - step size, default=1000
 *              @key {Number} [diff] - min difference between min and max values, default=5000
 **/
blocks['g-filter-param'] = function (data, env) {
    assertHas(data, 'type', 'g-filter-param: Should point type');
    assertHas(data, 'title', 'g-filter-param: Should point title');
    assertHas(data, 'value', 'g-filter-param: Should point value for ' + data.type);
    state = data.state || 'opened';

    if (data.type == 'price') {
        var min = data.value.min;
        var max = data.value.max;
        var scale = data.value.scale || 162;
        var ticks = data.value.ticks || 2;
        var step = data.value.step || Math.round(max / 100);

        return {
            block: 'g-filter-param',
            mods: {
                'type': 'price',
                'state': state
            },
            type: data.type,
            title: data.title,
            min: min,
            max: max,
            scale: scale,
            ticks: ticks,
            step: step
        };
    }

    return {
        block: 'g-filter-param',
        mods: {
            'type': 'checkbox',
            'state': state
        },
        type: data.type,
        title: data.title,
        content: data.value
    };
};
