modules.define('g-price-control',
    ['i-bem__dom', 'router'],
    function (provide, BEMDOM, router) {

    BEMDOM.decl('g-price-control', {
        onSetMod: {
            js: {
                "inited": function () {
                    var self = this;
                    var minInput = this.findBlockInside({
                        block: 'g-input',
                        modName: 'min',
                        modVal: true
                    });
                    var maxInput = this.findBlockInside({
                        block: 'g-input',
                        modName: 'max',
                        modVal: true
                    });
                    var range = this.findBlockInside('g-range');
                    var min = range.correctMinValue(+router.getParam('min'));
                    var max = range.correctMaxValue(+router.getParam('max'));

                    max = max < min ? range.params.max : max;

                    minInput.setVal(min);
                    maxInput.setVal(max);
                    range.setMinValue(min);
                    range.setMaxValue(max);

                    this._blocks = {
                        minInput: minInput,
                        maxInput: maxInput,
                        range: range
                    };

                    minInput.on('change', function (e, value) {
                        range.setMinValue(value);
                    });

                    minInput.on('change', function (e, value) {
                        value = Math.round(value);
                        range.params.min == value && (value = null);
                        self.emit('changeMinPrice', {
                            type: 'min',
                            ids: value
                        });
                    });

                    maxInput.on('change', function (e, value) {
                        range.setMaxValue(value);
                    });

                    maxInput.on('change', function (e, value) {
                        value = Math.round(value);
                        range.params.max == value && (value = null);
                        self.emit('changeMaxPrice', {
                            type: 'max',
                            ids: value
                        });
                    });

                    range.on('change', function (e, data) {
                        minInput.setVal(data.min);
                        maxInput.setVal(data.max);
                    });
                }
            }
        }
    }, {});

    provide(BEMDOM);
});
