//! env node
// vim: ts=2: sw=2:
var fs = require('fs');
var builder = require('xmlbuilder');
var Q = require('q');
var sprintf = require('sprintf');
var _ = require('lodash');

var db = require('../../backend/src/db');
var HOST = 'http://goldpoisk.ru';
var ITEMS_ON_PAGE = 30;

main();

function main () {
  console.log('Generate sitemap.xml');
  console.time('process');
  var root = builder.create('root', {
    version: '1.0',
    encoding: 'UTF-8',
  });

  root.ele('urlset', {
    xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9'
  });

  appendSpecifics(db, root)
  .then(function () { return appendCategories(db, root) })
  .then(function () { return appendProducts(db, root) })
  .then(function () {
      fs.writeFileSync('sitemap.xml', root);
    })
    .fail(function (e) {
      console.error(e);
    })
  .finally(function () {
    console.timeEnd('process');
    db.sequelize.close();
  })
}


// utils
function appendSpecifics(db, root) {
  return Q.Promise(function (resolve, reject) {
      console.time('appendSpecifics');
      // path: /
      var node = root.ele('url');
      node.ele('loc', null, sprintf('%s/', HOST));
      //TODO: #269
      node.ele('lastmod', null, new Date());
      node.ele('priority', null, 0.7);

      // path: /best
      var node = root.ele('url');
      node.ele('loc', null, sprintf('%s/best', HOST));
      //TODO: #269
      node.ele('lastmod', null, new Date());
      node.ele('priority', null, 0.7);

      console.timeEnd('appendSpecifics');
      resolve();
  });
}

function appendCategories(db, root) {
  return Q.Promise(function (_resolve, _reject) {
    console.time('appendCategories');
    db.Type.getMenu()
    .then(function (menu) {
      node = root.ele('url')
      Q.all(_.map(menu, function (category) {
          node.ele('loc', null, sprintf('%s/%s', HOST, category.href));
          //TODO: #269
          node.ele('lastmod', null, new Date());
          node.ele('priority', null, 0.5);
          return appendPages(db, root, category);
        })
      )
      .then(resolve)
      .fail(reject);
    })
    .fail(reject)

    function resolve () {
      console.timeEnd('appendCategories');
      _resolve();
    }

    function reject () {
      console.timeEnd('appendCategories');
      _reject();
    }
  });
}

function appendPages(db, root, category) {
  return Q.Promise(function (resolve, reject) {
    db.Product.model.findAll({
      include: [{
        model: db.Item.model,
        require: true
      }, {
        model: db.Type.model,
        require: true,
        where: { name: category.type }
      }]
    })
    .then(function (resp) {
      // skip first page
      var page = 1;
      var offset = page * ITEMS_ON_PAGE;
      products = resp.slice(offset, ITEMS_ON_PAGE + offset);
      while (products.length) {
        var node = root.ele('url');
        node.ele('loc', null, sprintf('%s/%s?page=%d', HOST, category.href, page));
        //TODO: #269
        node.ele('lastmod', null, new Date());
        node.ele('priority', null, 0.4);

        page++;
        offset = page * ITEMS_ON_PAGE;
        products = resp.slice(offset, ITEMS_ON_PAGE + offset);
      }

      resolve();
    })
    .error(reject)
  });
}

function appendProducts(db, root) {
  return Q.Promise(function (resolve, reject) {
    console.time('appendProducts');
    db.Product.model.findAll({
      include: [{
        model: db.Item.model,
        require: true
      }, {
        model: db.Type.model,
        require: true
      }]
    })
    .then(function (products) {
      _.forEach(products, function (product) {
        var node = root.ele('url');
        node.ele('loc', null, sprintf('%s%s', HOST, product.getUrl()) );
        //TODO: #269
        node.ele('lastmod', null, new Date());
        node.ele('priority', null, 0.6);
      });

      console.timeEnd('appendProducts');
      resolve();
    })
    .error(reject)
  })
}
