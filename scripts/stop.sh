#!/bin/bash
name=goldpoisk
if [ "$DEPLOYMENT_GROUP_NAME" == "Staging" ]; then
    name=goldpoisk_stage
fi

pm2 stop $name
exit 0
