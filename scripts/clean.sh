#!/bin/bash
name=goldpoisk
if [ "$DEPLOYMENT_GROUP_NAME" == "Staging" ]; then
    name=goldpoisk_stage
fi
root=/webapps/${name}

sed -ie "s/\/webapps\/goldpoisk/\/webapps\/${name}/" appspec.yml
rm -rf $root
