#!/bin/bash
set -x
name=goldpoisk
PORT=3000
if [ "$DEPLOYMENT_GROUP_NAME" == "Staging" ]; then
    name=goldpoisk_stage
    PORT=5000
fi
pidfile=/home/ec2-user/run/${name}.pid
logfile=/home/ec2-user/log/${name}.log
root=/webapps/${name}

# models
echo '>>models'
cd $root/backend/src/modules/models
npm i
#sitemap
echo '>>sitemap'
cd $root/scripts/utils/
npm i
node --harmony_destructuring ./sitemap.js
cp -v sitemap.xml $root/public/

#backend
cd $root/backend/
npm i
PORT=$port pm2 start --name $name --pid $pidfile --log $logfile --node-args="--harmony_destructuring" src/app.js
