--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.5
-- Dumped by pg_dump version 9.4.1
-- Started on 2016-03-03 23:52:09 MSK

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 6 (class 2615 OID 16395)
-- Name: dev_goldpoisk_test; Type: SCHEMA; Schema: -; Owner: dev_goldpoisk
--

CREATE SCHEMA dev_goldpoisk_test;


ALTER SCHEMA dev_goldpoisk_test OWNER TO dev_goldpoisk;

--
-- TOC entry 3274 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA dev_goldpoisk_test; Type: COMMENT; Schema: -; Owner: dev_goldpoisk
--

COMMENT ON SCHEMA dev_goldpoisk_test IS 'Тестовая база для выгрузки данных из интернета';


--
-- TOC entry 243 (class 3079 OID 12723)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3278 (class 0 OID 0)
-- Dependencies: 243
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 225 (class 1259 OID 16807)
-- Name: Articles; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE "Articles" (
    id integer NOT NULL,
    title character varying(255),
    url character varying(255),
    text character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE "Articles" OWNER TO dev_goldpoisk;

--
-- TOC entry 224 (class 1259 OID 16805)
-- Name: Articles_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE "Articles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Articles_id_seq" OWNER TO dev_goldpoisk;

--
-- TOC entry 3279 (class 0 OID 0)
-- Dependencies: 224
-- Name: Articles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE "Articles_id_seq" OWNED BY "Articles".id;


--
-- TOC entry 236 (class 1259 OID 16865)
-- Name: Banners; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE "Banners" (
    id integer NOT NULL,
    name character varying(128),
    image character varying(128),
    hidden boolean
);


ALTER TABLE "Banners" OWNER TO dev_goldpoisk;

--
-- TOC entry 235 (class 1259 OID 16863)
-- Name: Banners_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE "Banners_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Banners_id_seq" OWNER TO dev_goldpoisk;

--
-- TOC entry 3280 (class 0 OID 0)
-- Dependencies: 235
-- Name: Banners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE "Banners_id_seq" OWNED BY "Banners".id;


--
-- TOC entry 233 (class 1259 OID 16842)
-- Name: BestBids; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE "BestBids" (
    id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE "BestBids" OWNER TO dev_goldpoisk;

--
-- TOC entry 232 (class 1259 OID 16840)
-- Name: BestBids_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE "BestBids_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "BestBids_id_seq" OWNER TO dev_goldpoisk;

--
-- TOC entry 3281 (class 0 OID 0)
-- Dependencies: 232
-- Name: BestBids_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE "BestBids_id_seq" OWNED BY "BestBids".id;


--
-- TOC entry 231 (class 1259 OID 16834)
-- Name: Materials; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE "Materials" (
    id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE "Materials" OWNER TO dev_goldpoisk;

--
-- TOC entry 230 (class 1259 OID 16832)
-- Name: Materials_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE "Materials_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Materials_id_seq" OWNER TO dev_goldpoisk;

--
-- TOC entry 3282 (class 0 OID 0)
-- Dependencies: 230
-- Name: Materials_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE "Materials_id_seq" OWNED BY "Materials".id;


--
-- TOC entry 227 (class 1259 OID 16818)
-- Name: Products; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE "Products" (
    id integer NOT NULL,
    type integer,
    name character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE "Products" OWNER TO dev_goldpoisk;

--
-- TOC entry 226 (class 1259 OID 16816)
-- Name: Products_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE "Products_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Products_id_seq" OWNER TO dev_goldpoisk;

--
-- TOC entry 3283 (class 0 OID 0)
-- Dependencies: 226
-- Name: Products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE "Products_id_seq" OWNED BY "Products".id;


--
-- TOC entry 238 (class 1259 OID 16873)
-- Name: Promotions; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE "Promotions" (
    id integer NOT NULL,
    x numeric,
    y numeric,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE "Promotions" OWNER TO dev_goldpoisk;

--
-- TOC entry 237 (class 1259 OID 16871)
-- Name: Promotions_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE "Promotions_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Promotions_id_seq" OWNER TO dev_goldpoisk;

--
-- TOC entry 3284 (class 0 OID 0)
-- Dependencies: 237
-- Name: Promotions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE "Promotions_id_seq" OWNED BY "Promotions".id;


--
-- TOC entry 240 (class 1259 OID 16885)
-- Name: Shops; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE "Shops" (
    id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE "Shops" OWNER TO dev_goldpoisk;

--
-- TOC entry 239 (class 1259 OID 16883)
-- Name: Shops_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE "Shops_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Shops_id_seq" OWNER TO dev_goldpoisk;

--
-- TOC entry 3285 (class 0 OID 0)
-- Dependencies: 239
-- Name: Shops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE "Shops_id_seq" OWNED BY "Shops".id;


--
-- TOC entry 229 (class 1259 OID 16826)
-- Name: Types; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE "Types" (
    id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE "Types" OWNER TO dev_goldpoisk;

--
-- TOC entry 228 (class 1259 OID 16824)
-- Name: Types_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE "Types_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Types_id_seq" OWNER TO dev_goldpoisk;

--
-- TOC entry 3286 (class 0 OID 0)
-- Dependencies: 228
-- Name: Types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE "Types_id_seq" OWNED BY "Types".id;


--
-- TOC entry 173 (class 1259 OID 16396)
-- Name: auth_group; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO dev_goldpoisk;

--
-- TOC entry 174 (class 1259 OID 16399)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3288 (class 0 OID 0)
-- Dependencies: 174
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- TOC entry 175 (class 1259 OID 16401)
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO dev_goldpoisk;

--
-- TOC entry 176 (class 1259 OID 16404)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3291 (class 0 OID 0)
-- Dependencies: 176
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- TOC entry 177 (class 1259 OID 16406)
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO dev_goldpoisk;

--
-- TOC entry 178 (class 1259 OID 16409)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3294 (class 0 OID 0)
-- Dependencies: 178
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- TOC entry 179 (class 1259 OID 16411)
-- Name: auth_user; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO dev_goldpoisk;

--
-- TOC entry 180 (class 1259 OID 16414)
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO dev_goldpoisk;

--
-- TOC entry 181 (class 1259 OID 16417)
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3298 (class 0 OID 0)
-- Dependencies: 181
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- TOC entry 182 (class 1259 OID 16419)
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3300 (class 0 OID 0)
-- Dependencies: 182
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- TOC entry 183 (class 1259 OID 16421)
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO dev_goldpoisk;

--
-- TOC entry 184 (class 1259 OID 16424)
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3303 (class 0 OID 0)
-- Dependencies: 184
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- TOC entry 185 (class 1259 OID 16426)
-- Name: cms_banner; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE cms_banner (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    image character varying(100) NOT NULL,
    hidden boolean NOT NULL
);


ALTER TABLE cms_banner OWNER TO dev_goldpoisk;

--
-- TOC entry 186 (class 1259 OID 16429)
-- Name: cms_banner_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE cms_banner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_banner_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3305 (class 0 OID 0)
-- Dependencies: 186
-- Name: cms_banner_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE cms_banner_id_seq OWNED BY cms_banner.id;


--
-- TOC entry 187 (class 1259 OID 16431)
-- Name: cms_promotion; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE cms_promotion (
    id integer NOT NULL,
    banner_id integer NOT NULL,
    x numeric(5,2) NOT NULL,
    y numeric(5,2) NOT NULL,
    item_id integer NOT NULL
);


ALTER TABLE cms_promotion OWNER TO dev_goldpoisk;

--
-- TOC entry 188 (class 1259 OID 16434)
-- Name: cms_promotion_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE cms_promotion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cms_promotion_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3306 (class 0 OID 0)
-- Dependencies: 188
-- Name: cms_promotion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE cms_promotion_id_seq OWNED BY cms_promotion.id;


--
-- TOC entry 189 (class 1259 OID 16436)
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO dev_goldpoisk;

--
-- TOC entry 190 (class 1259 OID 16443)
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3308 (class 0 OID 0)
-- Dependencies: 190
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- TOC entry 191 (class 1259 OID 16445)
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO dev_goldpoisk;

--
-- TOC entry 192 (class 1259 OID 16448)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3311 (class 0 OID 0)
-- Dependencies: 192
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- TOC entry 193 (class 1259 OID 16450)
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO dev_goldpoisk;

--
-- TOC entry 194 (class 1259 OID 16456)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3314 (class 0 OID 0)
-- Dependencies: 194
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- TOC entry 195 (class 1259 OID 16458)
-- Name: django_session; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO dev_goldpoisk;

--
-- TOC entry 196 (class 1259 OID 16464)
-- Name: goldpoisk_action; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE goldpoisk_action (
    id integer NOT NULL,
    item_id integer NOT NULL
);


ALTER TABLE goldpoisk_action OWNER TO dev_goldpoisk;

--
-- TOC entry 197 (class 1259 OID 16467)
-- Name: goldpoisk_action_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE goldpoisk_action_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE goldpoisk_action_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3318 (class 0 OID 0)
-- Dependencies: 197
-- Name: goldpoisk_action_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE goldpoisk_action_id_seq OWNED BY goldpoisk_action.id;


--
-- TOC entry 198 (class 1259 OID 16469)
-- Name: goldpoisk_bestbid; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE goldpoisk_bestbid (
    id integer NOT NULL,
    item_id integer NOT NULL
);


ALTER TABLE goldpoisk_bestbid OWNER TO dev_goldpoisk;

--
-- TOC entry 199 (class 1259 OID 16472)
-- Name: goldpoisk_bestbid_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE goldpoisk_bestbid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE goldpoisk_bestbid_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3321 (class 0 OID 0)
-- Dependencies: 199
-- Name: goldpoisk_bestbid_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE goldpoisk_bestbid_id_seq OWNED BY goldpoisk_bestbid.id;


--
-- TOC entry 200 (class 1259 OID 16474)
-- Name: goldpoisk_hit; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE goldpoisk_hit (
    id integer NOT NULL,
    item_id integer NOT NULL
);


ALTER TABLE goldpoisk_hit OWNER TO dev_goldpoisk;

--
-- TOC entry 201 (class 1259 OID 16477)
-- Name: goldpoisk_hit_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE goldpoisk_hit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE goldpoisk_hit_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3324 (class 0 OID 0)
-- Dependencies: 201
-- Name: goldpoisk_hit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE goldpoisk_hit_id_seq OWNED BY goldpoisk_hit.id;


--
-- TOC entry 202 (class 1259 OID 16479)
-- Name: product_gem; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE product_gem (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    carat numeric(8,5) NOT NULL
);


ALTER TABLE product_gem OWNER TO dev_goldpoisk;

--
-- TOC entry 203 (class 1259 OID 16482)
-- Name: product_gem_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE product_gem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_gem_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3327 (class 0 OID 0)
-- Dependencies: 203
-- Name: product_gem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE product_gem_id_seq OWNED BY product_gem.id;


--
-- TOC entry 204 (class 1259 OID 16484)
-- Name: product_image; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE product_image (
    id integer NOT NULL,
    src character varying(100) NOT NULL,
    product_id integer NOT NULL
);


ALTER TABLE product_image OWNER TO dev_goldpoisk;

--
-- TOC entry 205 (class 1259 OID 16487)
-- Name: product_image_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE product_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_image_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3330 (class 0 OID 0)
-- Dependencies: 205
-- Name: product_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE product_image_id_seq OWNED BY product_image.id;


--
-- TOC entry 206 (class 1259 OID 16489)
-- Name: product_item; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE product_item (
    id integer NOT NULL,
    cost integer NOT NULL,
    quantity smallint NOT NULL,
    product_id integer NOT NULL,
    shop_id integer NOT NULL,
    buy_url character varying(256) NOT NULL,
    updated timestamp with time zone NOT NULL,
    CONSTRAINT product_item_cost_check CHECK ((cost >= 0)),
    CONSTRAINT product_item_quantity_check CHECK ((quantity >= 0))
);


ALTER TABLE product_item OWNER TO dev_goldpoisk;

--
-- TOC entry 207 (class 1259 OID 16494)
-- Name: product_item_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE product_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_item_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3333 (class 0 OID 0)
-- Dependencies: 207
-- Name: product_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE product_item_id_seq OWNED BY product_item.id;


--
-- TOC entry 208 (class 1259 OID 16496)
-- Name: product_material; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE product_material (
    id integer NOT NULL,
    name character varying(128) NOT NULL
);


ALTER TABLE product_material OWNER TO dev_goldpoisk;

--
-- TOC entry 209 (class 1259 OID 16499)
-- Name: product_material_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE product_material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_material_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3336 (class 0 OID 0)
-- Dependencies: 209
-- Name: product_material_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE product_material_id_seq OWNED BY product_material.id;


--
-- TOC entry 210 (class 1259 OID 16501)
-- Name: product_product; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE product_product (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    description text NOT NULL,
    number character varying(32) NOT NULL,
    weight numeric(8,5) NOT NULL,
    type_id integer NOT NULL,
    slug character varying(50) NOT NULL
);


ALTER TABLE product_product OWNER TO dev_goldpoisk;

--
-- TOC entry 234 (class 1259 OID 16848)
-- Name: product_product_; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE product_product_ (
    product_id integer NOT NULL,
    "GemId" integer NOT NULL
);


ALTER TABLE product_product_ OWNER TO dev_goldpoisk;

--
-- TOC entry 211 (class 1259 OID 16507)
-- Name: product_product_gems; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE product_product_gems (
    id integer NOT NULL,
    product_id integer NOT NULL,
    gem_id integer NOT NULL
);


ALTER TABLE product_product_gems OWNER TO dev_goldpoisk;

--
-- TOC entry 212 (class 1259 OID 16510)
-- Name: product_product_gems_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE product_product_gems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_product_gems_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3340 (class 0 OID 0)
-- Dependencies: 212
-- Name: product_product_gems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE product_product_gems_id_seq OWNED BY product_product_gems.id;


--
-- TOC entry 213 (class 1259 OID 16512)
-- Name: product_product_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE product_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_product_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3342 (class 0 OID 0)
-- Dependencies: 213
-- Name: product_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE product_product_id_seq OWNED BY product_product.id;


--
-- TOC entry 214 (class 1259 OID 16514)
-- Name: product_product_materials; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE product_product_materials (
    id integer NOT NULL,
    product_id integer NOT NULL,
    material_id integer NOT NULL
);


ALTER TABLE product_product_materials OWNER TO dev_goldpoisk;

--
-- TOC entry 215 (class 1259 OID 16517)
-- Name: product_product_materials_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE product_product_materials_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_product_materials_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3345 (class 0 OID 0)
-- Dependencies: 215
-- Name: product_product_materials_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE product_product_materials_id_seq OWNED BY product_product_materials.id;


--
-- TOC entry 216 (class 1259 OID 16519)
-- Name: product_type; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE product_type (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    url character varying(32) NOT NULL,
    type character varying(32) NOT NULL
);


ALTER TABLE product_type OWNER TO dev_goldpoisk;

--
-- TOC entry 217 (class 1259 OID 16522)
-- Name: product_type_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE product_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_type_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 217
-- Name: product_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE product_type_id_seq OWNED BY product_type.id;


--
-- TOC entry 242 (class 1259 OID 16893)
-- Name: shop; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE shop (
    id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE shop OWNER TO dev_goldpoisk;

--
-- TOC entry 218 (class 1259 OID 16524)
-- Name: shop_admin; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE shop_admin (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    email character varying(128) NOT NULL
);


ALTER TABLE shop_admin OWNER TO dev_goldpoisk;

--
-- TOC entry 219 (class 1259 OID 16527)
-- Name: shop_admin_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE shop_admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shop_admin_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3351 (class 0 OID 0)
-- Dependencies: 219
-- Name: shop_admin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE shop_admin_id_seq OWNED BY shop_admin.id;


--
-- TOC entry 241 (class 1259 OID 16891)
-- Name: shop_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE shop_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shop_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3353 (class 0 OID 0)
-- Dependencies: 241
-- Name: shop_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE shop_id_seq OWNED BY shop.id;


--
-- TOC entry 220 (class 1259 OID 16529)
-- Name: shop_manager; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE shop_manager (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    email character varying(128) NOT NULL,
    shop_id integer NOT NULL
);


ALTER TABLE shop_manager OWNER TO dev_goldpoisk;

--
-- TOC entry 221 (class 1259 OID 16532)
-- Name: shop_manager_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE shop_manager_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shop_manager_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3355 (class 0 OID 0)
-- Dependencies: 221
-- Name: shop_manager_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE shop_manager_id_seq OWNED BY shop_manager.id;


--
-- TOC entry 222 (class 1259 OID 16534)
-- Name: shop_shop; Type: TABLE; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE TABLE shop_shop (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    description text NOT NULL,
    admin_id integer NOT NULL,
    url character varying(256) NOT NULL,
    updated timestamp with time zone NOT NULL
);


ALTER TABLE shop_shop OWNER TO dev_goldpoisk;

--
-- TOC entry 223 (class 1259 OID 16540)
-- Name: shop_shop_id_seq; Type: SEQUENCE; Schema: public; Owner: dev_goldpoisk
--

CREATE SEQUENCE shop_shop_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shop_shop_id_seq OWNER TO dev_goldpoisk;

--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 223
-- Name: shop_shop_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dev_goldpoisk
--

ALTER SEQUENCE shop_shop_id_seq OWNED BY shop_shop.id;


--
-- TOC entry 2989 (class 2604 OID 16810)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY "Articles" ALTER COLUMN id SET DEFAULT nextval('"Articles_id_seq"'::regclass);


--
-- TOC entry 2994 (class 2604 OID 16868)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY "Banners" ALTER COLUMN id SET DEFAULT nextval('"Banners_id_seq"'::regclass);


--
-- TOC entry 2993 (class 2604 OID 16845)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY "BestBids" ALTER COLUMN id SET DEFAULT nextval('"BestBids_id_seq"'::regclass);


--
-- TOC entry 2992 (class 2604 OID 16837)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY "Materials" ALTER COLUMN id SET DEFAULT nextval('"Materials_id_seq"'::regclass);


--
-- TOC entry 2990 (class 2604 OID 16821)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY "Products" ALTER COLUMN id SET DEFAULT nextval('"Products_id_seq"'::regclass);


--
-- TOC entry 2995 (class 2604 OID 16876)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY "Promotions" ALTER COLUMN id SET DEFAULT nextval('"Promotions_id_seq"'::regclass);


--
-- TOC entry 2996 (class 2604 OID 16888)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY "Shops" ALTER COLUMN id SET DEFAULT nextval('"Shops_id_seq"'::regclass);


--
-- TOC entry 2991 (class 2604 OID 16829)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY "Types" ALTER COLUMN id SET DEFAULT nextval('"Types_id_seq"'::regclass);


--
-- TOC entry 2961 (class 2604 OID 16542)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- TOC entry 2962 (class 2604 OID 16543)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- TOC entry 2963 (class 2604 OID 16544)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- TOC entry 2964 (class 2604 OID 16545)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- TOC entry 2965 (class 2604 OID 16546)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- TOC entry 2966 (class 2604 OID 16547)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- TOC entry 2967 (class 2604 OID 16548)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY cms_banner ALTER COLUMN id SET DEFAULT nextval('cms_banner_id_seq'::regclass);


--
-- TOC entry 2968 (class 2604 OID 16549)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY cms_promotion ALTER COLUMN id SET DEFAULT nextval('cms_promotion_id_seq'::regclass);


--
-- TOC entry 2969 (class 2604 OID 16550)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- TOC entry 2971 (class 2604 OID 16551)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- TOC entry 2972 (class 2604 OID 16552)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- TOC entry 2973 (class 2604 OID 16553)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY goldpoisk_action ALTER COLUMN id SET DEFAULT nextval('goldpoisk_action_id_seq'::regclass);


--
-- TOC entry 2974 (class 2604 OID 16554)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY goldpoisk_bestbid ALTER COLUMN id SET DEFAULT nextval('goldpoisk_bestbid_id_seq'::regclass);


--
-- TOC entry 2975 (class 2604 OID 16555)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY goldpoisk_hit ALTER COLUMN id SET DEFAULT nextval('goldpoisk_hit_id_seq'::regclass);


--
-- TOC entry 2976 (class 2604 OID 16556)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_gem ALTER COLUMN id SET DEFAULT nextval('product_gem_id_seq'::regclass);


--
-- TOC entry 2977 (class 2604 OID 16557)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_image ALTER COLUMN id SET DEFAULT nextval('product_image_id_seq'::regclass);


--
-- TOC entry 2978 (class 2604 OID 16558)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_item ALTER COLUMN id SET DEFAULT nextval('product_item_id_seq'::regclass);


--
-- TOC entry 2981 (class 2604 OID 16559)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_material ALTER COLUMN id SET DEFAULT nextval('product_material_id_seq'::regclass);


--
-- TOC entry 2982 (class 2604 OID 16560)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_product ALTER COLUMN id SET DEFAULT nextval('product_product_id_seq'::regclass);


--
-- TOC entry 2983 (class 2604 OID 16561)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_product_gems ALTER COLUMN id SET DEFAULT nextval('product_product_gems_id_seq'::regclass);


--
-- TOC entry 2984 (class 2604 OID 16562)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_product_materials ALTER COLUMN id SET DEFAULT nextval('product_product_materials_id_seq'::regclass);


--
-- TOC entry 2985 (class 2604 OID 16563)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_type ALTER COLUMN id SET DEFAULT nextval('product_type_id_seq'::regclass);


--
-- TOC entry 2997 (class 2604 OID 16896)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY shop ALTER COLUMN id SET DEFAULT nextval('shop_id_seq'::regclass);


--
-- TOC entry 2986 (class 2604 OID 16564)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY shop_admin ALTER COLUMN id SET DEFAULT nextval('shop_admin_id_seq'::regclass);


--
-- TOC entry 2987 (class 2604 OID 16565)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY shop_manager ALTER COLUMN id SET DEFAULT nextval('shop_manager_id_seq'::regclass);


--
-- TOC entry 2988 (class 2604 OID 16566)
-- Name: id; Type: DEFAULT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY shop_shop ALTER COLUMN id SET DEFAULT nextval('shop_shop_id_seq'::regclass);


--
-- TOC entry 3116 (class 2606 OID 16815)
-- Name: Articles_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY "Articles"
    ADD CONSTRAINT "Articles_pkey" PRIMARY KEY (id);


--
-- TOC entry 3128 (class 2606 OID 16870)
-- Name: Banners_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY "Banners"
    ADD CONSTRAINT "Banners_pkey" PRIMARY KEY (id);


--
-- TOC entry 3124 (class 2606 OID 16847)
-- Name: BestBids_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY "BestBids"
    ADD CONSTRAINT "BestBids_pkey" PRIMARY KEY (id);


--
-- TOC entry 3122 (class 2606 OID 16839)
-- Name: Materials_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY "Materials"
    ADD CONSTRAINT "Materials_pkey" PRIMARY KEY (id);


--
-- TOC entry 3118 (class 2606 OID 16823)
-- Name: Products_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY "Products"
    ADD CONSTRAINT "Products_pkey" PRIMARY KEY (id);


--
-- TOC entry 3130 (class 2606 OID 16881)
-- Name: Promotions_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY "Promotions"
    ADD CONSTRAINT "Promotions_pkey" PRIMARY KEY (id);


--
-- TOC entry 3132 (class 2606 OID 16890)
-- Name: Shops_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY "Shops"
    ADD CONSTRAINT "Shops_pkey" PRIMARY KEY (id);


--
-- TOC entry 3120 (class 2606 OID 16831)
-- Name: Types_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY "Types"
    ADD CONSTRAINT "Types_pkey" PRIMARY KEY (id);


--
-- TOC entry 3000 (class 2606 OID 16568)
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 3006 (class 2606 OID 16570)
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- TOC entry 3008 (class 2606 OID 16572)
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3002 (class 2606 OID 16574)
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 3011 (class 2606 OID 16576)
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- TOC entry 3013 (class 2606 OID 16578)
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 3022 (class 2606 OID 16580)
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 3024 (class 2606 OID 16582)
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- TOC entry 3015 (class 2606 OID 16584)
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- TOC entry 3028 (class 2606 OID 16586)
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3030 (class 2606 OID 16588)
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- TOC entry 3018 (class 2606 OID 16590)
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- TOC entry 3032 (class 2606 OID 16592)
-- Name: cms_banner_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY cms_banner
    ADD CONSTRAINT cms_banner_pkey PRIMARY KEY (id);


--
-- TOC entry 3036 (class 2606 OID 16594)
-- Name: cms_promotion_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY cms_promotion
    ADD CONSTRAINT cms_promotion_pkey PRIMARY KEY (id);


--
-- TOC entry 3040 (class 2606 OID 16596)
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3042 (class 2606 OID 16598)
-- Name: django_content_type_app_label_45f3b1d93ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_45f3b1d93ec8c61c_uniq UNIQUE (app_label, model);


--
-- TOC entry 3044 (class 2606 OID 16600)
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3046 (class 2606 OID 16602)
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 3049 (class 2606 OID 16604)
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 3052 (class 2606 OID 16606)
-- Name: goldpoisk_action_item_id_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY goldpoisk_action
    ADD CONSTRAINT goldpoisk_action_item_id_key UNIQUE (item_id);


--
-- TOC entry 3054 (class 2606 OID 16608)
-- Name: goldpoisk_action_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY goldpoisk_action
    ADD CONSTRAINT goldpoisk_action_pkey PRIMARY KEY (id);


--
-- TOC entry 3056 (class 2606 OID 16610)
-- Name: goldpoisk_bestbid_item_id_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY goldpoisk_bestbid
    ADD CONSTRAINT goldpoisk_bestbid_item_id_key UNIQUE (item_id);


--
-- TOC entry 3058 (class 2606 OID 16612)
-- Name: goldpoisk_bestbid_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY goldpoisk_bestbid
    ADD CONSTRAINT goldpoisk_bestbid_pkey PRIMARY KEY (id);


--
-- TOC entry 3060 (class 2606 OID 16614)
-- Name: goldpoisk_hit_item_id_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY goldpoisk_hit
    ADD CONSTRAINT goldpoisk_hit_item_id_key UNIQUE (item_id);


--
-- TOC entry 3062 (class 2606 OID 16616)
-- Name: goldpoisk_hit_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY goldpoisk_hit
    ADD CONSTRAINT goldpoisk_hit_pkey PRIMARY KEY (id);


--
-- TOC entry 3064 (class 2606 OID 16618)
-- Name: product_gem_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_gem
    ADD CONSTRAINT product_gem_pkey PRIMARY KEY (id);


--
-- TOC entry 3066 (class 2606 OID 16620)
-- Name: product_image_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_image
    ADD CONSTRAINT product_image_pkey PRIMARY KEY (id);


--
-- TOC entry 3071 (class 2606 OID 16622)
-- Name: product_item_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_item
    ADD CONSTRAINT product_item_pkey PRIMARY KEY (id);


--
-- TOC entry 3075 (class 2606 OID 16624)
-- Name: product_material_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_material
    ADD CONSTRAINT product_material_pkey PRIMARY KEY (id);


--
-- TOC entry 3126 (class 2606 OID 16852)
-- Name: product_product__pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_product_
    ADD CONSTRAINT product_product__pkey PRIMARY KEY (product_id, "GemId");


--
-- TOC entry 3083 (class 2606 OID 16626)
-- Name: product_product_gems_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_product_gems
    ADD CONSTRAINT product_product_gems_pkey PRIMARY KEY (id);


--
-- TOC entry 3086 (class 2606 OID 16628)
-- Name: product_product_gems_product_id_gem_id_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_product_gems
    ADD CONSTRAINT product_product_gems_product_id_gem_id_key UNIQUE (product_id, gem_id);


--
-- TOC entry 3089 (class 2606 OID 16630)
-- Name: product_product_materials_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_product_materials
    ADD CONSTRAINT product_product_materials_pkey PRIMARY KEY (id);


--
-- TOC entry 3092 (class 2606 OID 16632)
-- Name: product_product_materials_product_id_material_id_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_product_materials
    ADD CONSTRAINT product_product_materials_product_id_material_id_key UNIQUE (product_id, material_id);


--
-- TOC entry 3079 (class 2606 OID 16634)
-- Name: product_product_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_product
    ADD CONSTRAINT product_product_pkey PRIMARY KEY (id);


--
-- TOC entry 3095 (class 2606 OID 16636)
-- Name: product_type_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_type
    ADD CONSTRAINT product_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3097 (class 2606 OID 16638)
-- Name: product_type_type_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY product_type
    ADD CONSTRAINT product_type_type_key UNIQUE (type);


--
-- TOC entry 3102 (class 2606 OID 16640)
-- Name: shop_admin_email_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY shop_admin
    ADD CONSTRAINT shop_admin_email_key UNIQUE (email);


--
-- TOC entry 3105 (class 2606 OID 16642)
-- Name: shop_admin_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY shop_admin
    ADD CONSTRAINT shop_admin_pkey PRIMARY KEY (id);


--
-- TOC entry 3107 (class 2606 OID 16644)
-- Name: shop_manager_email_key; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY shop_manager
    ADD CONSTRAINT shop_manager_email_key UNIQUE (email);


--
-- TOC entry 3110 (class 2606 OID 16646)
-- Name: shop_manager_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY shop_manager
    ADD CONSTRAINT shop_manager_pkey PRIMARY KEY (id);


--
-- TOC entry 3134 (class 2606 OID 16898)
-- Name: shop_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY shop
    ADD CONSTRAINT shop_pkey PRIMARY KEY (id);


--
-- TOC entry 3114 (class 2606 OID 16648)
-- Name: shop_shop_pkey; Type: CONSTRAINT; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

ALTER TABLE ONLY shop_shop
    ADD CONSTRAINT shop_shop_pkey PRIMARY KEY (id);


--
-- TOC entry 2998 (class 1259 OID 16649)
-- Name: auth_group_name_253ae2a6331666e8_like; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX auth_group_name_253ae2a6331666e8_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 3003 (class 1259 OID 16650)
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- TOC entry 3004 (class 1259 OID 16651)
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- TOC entry 3009 (class 1259 OID 16652)
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- TOC entry 3019 (class 1259 OID 16653)
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- TOC entry 3020 (class 1259 OID 16654)
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- TOC entry 3025 (class 1259 OID 16655)
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- TOC entry 3026 (class 1259 OID 16656)
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- TOC entry 3016 (class 1259 OID 16657)
-- Name: auth_user_username_51b3b110094b8aae_like; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX auth_user_username_51b3b110094b8aae_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- TOC entry 3033 (class 1259 OID 16658)
-- Name: cms_promotion_banner_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX cms_promotion_banner_id ON cms_promotion USING btree (banner_id);


--
-- TOC entry 3034 (class 1259 OID 16659)
-- Name: cms_promotion_item_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX cms_promotion_item_id ON cms_promotion USING btree (item_id);


--
-- TOC entry 3037 (class 1259 OID 16660)
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- TOC entry 3038 (class 1259 OID 16661)
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- TOC entry 3047 (class 1259 OID 16662)
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- TOC entry 3050 (class 1259 OID 16663)
-- Name: django_session_session_key_461cfeaa630ca218_like; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX django_session_session_key_461cfeaa630ca218_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 3067 (class 1259 OID 16664)
-- Name: product_image_product_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_image_product_id ON product_image USING btree (product_id);


--
-- TOC entry 3068 (class 1259 OID 16665)
-- Name: product_item_9bea82de; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_item_9bea82de ON product_item USING btree (product_id);


--
-- TOC entry 3069 (class 1259 OID 16666)
-- Name: product_item_9f32e8f6; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_item_9f32e8f6 ON product_item USING btree (shop_id);


--
-- TOC entry 3072 (class 1259 OID 16667)
-- Name: product_item_product_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_item_product_id ON product_item USING btree (product_id);


--
-- TOC entry 3073 (class 1259 OID 16668)
-- Name: product_item_shop_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_item_shop_id ON product_item USING btree (shop_id);


--
-- TOC entry 3076 (class 1259 OID 16669)
-- Name: product_product_2dbcba41; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_product_2dbcba41 ON product_product USING btree (slug);


--
-- TOC entry 3077 (class 1259 OID 16670)
-- Name: product_product_94757cae; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_product_94757cae ON product_product USING btree (type_id);


--
-- TOC entry 3081 (class 1259 OID 16671)
-- Name: product_product_gems_gem_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_product_gems_gem_id ON product_product_gems USING btree (gem_id);


--
-- TOC entry 3084 (class 1259 OID 16673)
-- Name: product_product_gems_product_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_product_gems_product_id ON product_product_gems USING btree (product_id);


--
-- TOC entry 3087 (class 1259 OID 16675)
-- Name: product_product_materials_material_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_product_materials_material_id ON product_product_materials USING btree (material_id);


--
-- TOC entry 3090 (class 1259 OID 16676)
-- Name: product_product_materials_product_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_product_materials_product_id ON product_product_materials USING btree (product_id);


--
-- TOC entry 3080 (class 1259 OID 16677)
-- Name: product_product_type_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_product_type_id ON product_product USING btree (type_id);


--
-- TOC entry 3093 (class 1259 OID 16679)
-- Name: product_type_572d4e42; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_type_572d4e42 ON product_type USING btree (url);


--
-- TOC entry 3098 (class 1259 OID 16680)
-- Name: product_type_type_like; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_type_type_like ON product_type USING btree (type varchar_pattern_ops);


--
-- TOC entry 3099 (class 1259 OID 16681)
-- Name: product_type_url; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_type_url ON product_type USING btree (url);


--
-- TOC entry 3100 (class 1259 OID 16682)
-- Name: product_type_url_like; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX product_type_url_like ON product_type USING btree (url varchar_pattern_ops);


--
-- TOC entry 3103 (class 1259 OID 16683)
-- Name: shop_admin_email_like; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX shop_admin_email_like ON shop_admin USING btree (email varchar_pattern_ops);


--
-- TOC entry 3108 (class 1259 OID 16684)
-- Name: shop_manager_email_like; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX shop_manager_email_like ON shop_manager USING btree (email varchar_pattern_ops);


--
-- TOC entry 3111 (class 1259 OID 16685)
-- Name: shop_manager_shop_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX shop_manager_shop_id ON shop_manager USING btree (shop_id);


--
-- TOC entry 3112 (class 1259 OID 16686)
-- Name: shop_shop_admin_id; Type: INDEX; Schema: public; Owner: dev_goldpoisk; Tablespace: 
--

CREATE INDEX shop_shop_admin_id ON shop_shop USING btree (admin_id);


--
-- TOC entry 3157 (class 2606 OID 16687)
-- Name: admin_id_refs_id_3c1b17c0; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY shop_shop
    ADD CONSTRAINT admin_id_refs_id_3c1b17c0 FOREIGN KEY (admin_id) REFERENCES shop_admin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3137 (class 2606 OID 16692)
-- Name: auth_content_type_id_508cf46651277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_content_type_id_508cf46651277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3135 (class 2606 OID 16697)
-- Name: auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3136 (class 2606 OID 16702)
-- Name: auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3140 (class 2606 OID 16707)
-- Name: auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3138 (class 2606 OID 16712)
-- Name: auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3139 (class 2606 OID 16717)
-- Name: auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3141 (class 2606 OID 16722)
-- Name: auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3142 (class 2606 OID 16727)
-- Name: cms_promotion_banner_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY cms_promotion
    ADD CONSTRAINT cms_promotion_banner_id_fkey FOREIGN KEY (banner_id) REFERENCES cms_banner(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3143 (class 2606 OID 16732)
-- Name: djan_content_type_id_697914295151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT djan_content_type_id_697914295151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3144 (class 2606 OID 16737)
-- Name: django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3147 (class 2606 OID 16742)
-- Name: item_id_refs_id_626db74e; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY goldpoisk_hit
    ADD CONSTRAINT item_id_refs_id_626db74e FOREIGN KEY (item_id) REFERENCES product_item(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3146 (class 2606 OID 16747)
-- Name: item_id_refs_id_b5855089; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY goldpoisk_bestbid
    ADD CONSTRAINT item_id_refs_id_b5855089 FOREIGN KEY (item_id) REFERENCES product_item(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3145 (class 2606 OID 16752)
-- Name: item_id_refs_id_f54439b4; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY goldpoisk_action
    ADD CONSTRAINT item_id_refs_id_f54439b4 FOREIGN KEY (item_id) REFERENCES product_item(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3155 (class 2606 OID 16757)
-- Name: material_id_refs_id_b89cd8a8; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_product_materials
    ADD CONSTRAINT material_id_refs_id_b89cd8a8 FOREIGN KEY (material_id) REFERENCES product_material(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3148 (class 2606 OID 16765)
-- Name: product_item_product_id_99e8a791ddc2779_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_item
    ADD CONSTRAINT product_item_product_id_99e8a791ddc2779_fk_product_product_id FOREIGN KEY (product_id) REFERENCES product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3149 (class 2606 OID 16770)
-- Name: product_item_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_item
    ADD CONSTRAINT product_item_product_id_fkey FOREIGN KEY (product_id) REFERENCES product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3150 (class 2606 OID 16775)
-- Name: product_item_shop_id_73199ad9b02ecea5_fk_shop_shop_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_item
    ADD CONSTRAINT product_item_shop_id_73199ad9b02ecea5_fk_shop_shop_id FOREIGN KEY (shop_id) REFERENCES shop_shop(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3159 (class 2606 OID 16858)
-- Name: product_product__GemId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_product_
    ADD CONSTRAINT "product_product__GemId_fkey" FOREIGN KEY ("GemId") REFERENCES product_gem(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3158 (class 2606 OID 16853)
-- Name: product_product__product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_product_
    ADD CONSTRAINT product_product__product_id_fkey FOREIGN KEY (product_id) REFERENCES product_product(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3154 (class 2606 OID 16780)
-- Name: product_product_gems_gem_id_588633785f83e7a0_fk_product_gem_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_product_gems
    ADD CONSTRAINT product_product_gems_gem_id_588633785f83e7a0_fk_product_gem_id FOREIGN KEY (gem_id) REFERENCES product_gem(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3152 (class 2606 OID 16785)
-- Name: product_product_type_id_140a66ad9a25ca58_fk_product_type_id; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_product
    ADD CONSTRAINT product_product_type_id_140a66ad9a25ca58_fk_product_type_id FOREIGN KEY (type_id) REFERENCES product_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3151 (class 2606 OID 16790)
-- Name: shop_id_refs_id_fb525742; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_item
    ADD CONSTRAINT shop_id_refs_id_fb525742 FOREIGN KEY (shop_id) REFERENCES shop_shop(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3156 (class 2606 OID 16795)
-- Name: shop_manager_shop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY shop_manager
    ADD CONSTRAINT shop_manager_shop_id_fkey FOREIGN KEY (shop_id) REFERENCES shop_shop(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3153 (class 2606 OID 16800)
-- Name: type_id_refs_id_db3edb08; Type: FK CONSTRAINT; Schema: public; Owner: dev_goldpoisk
--

ALTER TABLE ONLY product_product
    ADD CONSTRAINT type_id_refs_id_db3edb08 FOREIGN KEY (type_id) REFERENCES product_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3275 (class 0 OID 0)
-- Dependencies: 6
-- Name: dev_goldpoisk_test; Type: ACL; Schema: -; Owner: dev_goldpoisk
--

REVOKE ALL ON SCHEMA dev_goldpoisk_test FROM PUBLIC;
REVOKE ALL ON SCHEMA dev_goldpoisk_test FROM dev_goldpoisk;
GRANT ALL ON SCHEMA dev_goldpoisk_test TO dev_goldpoisk;


--
-- TOC entry 3277 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: dev_goldpoisk
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM dev_goldpoisk;
GRANT ALL ON SCHEMA public TO dev_goldpoisk;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 3287 (class 0 OID 0)
-- Dependencies: 173
-- Name: auth_group; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE auth_group FROM PUBLIC;
REVOKE ALL ON TABLE auth_group FROM dev_goldpoisk;
GRANT ALL ON TABLE auth_group TO dev_goldpoisk;


--
-- TOC entry 3289 (class 0 OID 0)
-- Dependencies: 174
-- Name: auth_group_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE auth_group_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE auth_group_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE auth_group_id_seq TO dev_goldpoisk;


--
-- TOC entry 3290 (class 0 OID 0)
-- Dependencies: 175
-- Name: auth_group_permissions; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE auth_group_permissions FROM PUBLIC;
REVOKE ALL ON TABLE auth_group_permissions FROM dev_goldpoisk;
GRANT ALL ON TABLE auth_group_permissions TO dev_goldpoisk;


--
-- TOC entry 3292 (class 0 OID 0)
-- Dependencies: 176
-- Name: auth_group_permissions_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE auth_group_permissions_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE auth_group_permissions_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE auth_group_permissions_id_seq TO dev_goldpoisk;


--
-- TOC entry 3293 (class 0 OID 0)
-- Dependencies: 177
-- Name: auth_permission; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE auth_permission FROM PUBLIC;
REVOKE ALL ON TABLE auth_permission FROM dev_goldpoisk;
GRANT ALL ON TABLE auth_permission TO dev_goldpoisk;


--
-- TOC entry 3295 (class 0 OID 0)
-- Dependencies: 178
-- Name: auth_permission_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE auth_permission_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE auth_permission_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE auth_permission_id_seq TO dev_goldpoisk;


--
-- TOC entry 3296 (class 0 OID 0)
-- Dependencies: 179
-- Name: auth_user; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE auth_user FROM PUBLIC;
REVOKE ALL ON TABLE auth_user FROM dev_goldpoisk;
GRANT ALL ON TABLE auth_user TO dev_goldpoisk;


--
-- TOC entry 3297 (class 0 OID 0)
-- Dependencies: 180
-- Name: auth_user_groups; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE auth_user_groups FROM PUBLIC;
REVOKE ALL ON TABLE auth_user_groups FROM dev_goldpoisk;
GRANT ALL ON TABLE auth_user_groups TO dev_goldpoisk;


--
-- TOC entry 3299 (class 0 OID 0)
-- Dependencies: 181
-- Name: auth_user_groups_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE auth_user_groups_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE auth_user_groups_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE auth_user_groups_id_seq TO dev_goldpoisk;


--
-- TOC entry 3301 (class 0 OID 0)
-- Dependencies: 182
-- Name: auth_user_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE auth_user_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE auth_user_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE auth_user_id_seq TO dev_goldpoisk;


--
-- TOC entry 3302 (class 0 OID 0)
-- Dependencies: 183
-- Name: auth_user_user_permissions; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE auth_user_user_permissions FROM PUBLIC;
REVOKE ALL ON TABLE auth_user_user_permissions FROM dev_goldpoisk;
GRANT ALL ON TABLE auth_user_user_permissions TO dev_goldpoisk;


--
-- TOC entry 3304 (class 0 OID 0)
-- Dependencies: 184
-- Name: auth_user_user_permissions_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE auth_user_user_permissions_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE auth_user_user_permissions_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE auth_user_user_permissions_id_seq TO dev_goldpoisk;


--
-- TOC entry 3307 (class 0 OID 0)
-- Dependencies: 189
-- Name: django_admin_log; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE django_admin_log FROM PUBLIC;
REVOKE ALL ON TABLE django_admin_log FROM dev_goldpoisk;
GRANT ALL ON TABLE django_admin_log TO dev_goldpoisk;


--
-- TOC entry 3309 (class 0 OID 0)
-- Dependencies: 190
-- Name: django_admin_log_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE django_admin_log_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE django_admin_log_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE django_admin_log_id_seq TO dev_goldpoisk;


--
-- TOC entry 3310 (class 0 OID 0)
-- Dependencies: 191
-- Name: django_content_type; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE django_content_type FROM PUBLIC;
REVOKE ALL ON TABLE django_content_type FROM dev_goldpoisk;
GRANT ALL ON TABLE django_content_type TO dev_goldpoisk;


--
-- TOC entry 3312 (class 0 OID 0)
-- Dependencies: 192
-- Name: django_content_type_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE django_content_type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE django_content_type_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE django_content_type_id_seq TO dev_goldpoisk;


--
-- TOC entry 3313 (class 0 OID 0)
-- Dependencies: 193
-- Name: django_migrations; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE django_migrations FROM PUBLIC;
REVOKE ALL ON TABLE django_migrations FROM dev_goldpoisk;
GRANT ALL ON TABLE django_migrations TO dev_goldpoisk;


--
-- TOC entry 3315 (class 0 OID 0)
-- Dependencies: 194
-- Name: django_migrations_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE django_migrations_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE django_migrations_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE django_migrations_id_seq TO dev_goldpoisk;


--
-- TOC entry 3316 (class 0 OID 0)
-- Dependencies: 195
-- Name: django_session; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE django_session FROM PUBLIC;
REVOKE ALL ON TABLE django_session FROM dev_goldpoisk;
GRANT ALL ON TABLE django_session TO dev_goldpoisk;


--
-- TOC entry 3317 (class 0 OID 0)
-- Dependencies: 196
-- Name: goldpoisk_action; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE goldpoisk_action FROM PUBLIC;
REVOKE ALL ON TABLE goldpoisk_action FROM dev_goldpoisk;
GRANT ALL ON TABLE goldpoisk_action TO dev_goldpoisk;


--
-- TOC entry 3319 (class 0 OID 0)
-- Dependencies: 197
-- Name: goldpoisk_action_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE goldpoisk_action_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE goldpoisk_action_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE goldpoisk_action_id_seq TO dev_goldpoisk;


--
-- TOC entry 3320 (class 0 OID 0)
-- Dependencies: 198
-- Name: goldpoisk_bestbid; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE goldpoisk_bestbid FROM PUBLIC;
REVOKE ALL ON TABLE goldpoisk_bestbid FROM dev_goldpoisk;
GRANT ALL ON TABLE goldpoisk_bestbid TO dev_goldpoisk;


--
-- TOC entry 3322 (class 0 OID 0)
-- Dependencies: 199
-- Name: goldpoisk_bestbid_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE goldpoisk_bestbid_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE goldpoisk_bestbid_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE goldpoisk_bestbid_id_seq TO dev_goldpoisk;


--
-- TOC entry 3323 (class 0 OID 0)
-- Dependencies: 200
-- Name: goldpoisk_hit; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE goldpoisk_hit FROM PUBLIC;
REVOKE ALL ON TABLE goldpoisk_hit FROM dev_goldpoisk;
GRANT ALL ON TABLE goldpoisk_hit TO dev_goldpoisk;


--
-- TOC entry 3325 (class 0 OID 0)
-- Dependencies: 201
-- Name: goldpoisk_hit_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE goldpoisk_hit_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE goldpoisk_hit_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE goldpoisk_hit_id_seq TO dev_goldpoisk;


--
-- TOC entry 3326 (class 0 OID 0)
-- Dependencies: 202
-- Name: product_gem; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE product_gem FROM PUBLIC;
REVOKE ALL ON TABLE product_gem FROM dev_goldpoisk;
GRANT ALL ON TABLE product_gem TO dev_goldpoisk;


--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 203
-- Name: product_gem_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE product_gem_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_gem_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE product_gem_id_seq TO dev_goldpoisk;


--
-- TOC entry 3329 (class 0 OID 0)
-- Dependencies: 204
-- Name: product_image; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE product_image FROM PUBLIC;
REVOKE ALL ON TABLE product_image FROM dev_goldpoisk;
GRANT ALL ON TABLE product_image TO dev_goldpoisk;


--
-- TOC entry 3331 (class 0 OID 0)
-- Dependencies: 205
-- Name: product_image_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE product_image_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_image_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE product_image_id_seq TO dev_goldpoisk;


--
-- TOC entry 3332 (class 0 OID 0)
-- Dependencies: 206
-- Name: product_item; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE product_item FROM PUBLIC;
REVOKE ALL ON TABLE product_item FROM dev_goldpoisk;
GRANT ALL ON TABLE product_item TO dev_goldpoisk;


--
-- TOC entry 3334 (class 0 OID 0)
-- Dependencies: 207
-- Name: product_item_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE product_item_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_item_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE product_item_id_seq TO dev_goldpoisk;


--
-- TOC entry 3335 (class 0 OID 0)
-- Dependencies: 208
-- Name: product_material; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE product_material FROM PUBLIC;
REVOKE ALL ON TABLE product_material FROM dev_goldpoisk;
GRANT ALL ON TABLE product_material TO dev_goldpoisk;


--
-- TOC entry 3337 (class 0 OID 0)
-- Dependencies: 209
-- Name: product_material_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE product_material_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_material_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE product_material_id_seq TO dev_goldpoisk;


--
-- TOC entry 3338 (class 0 OID 0)
-- Dependencies: 210
-- Name: product_product; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE product_product FROM PUBLIC;
REVOKE ALL ON TABLE product_product FROM dev_goldpoisk;
GRANT ALL ON TABLE product_product TO dev_goldpoisk;


--
-- TOC entry 3339 (class 0 OID 0)
-- Dependencies: 211
-- Name: product_product_gems; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE product_product_gems FROM PUBLIC;
REVOKE ALL ON TABLE product_product_gems FROM dev_goldpoisk;
GRANT ALL ON TABLE product_product_gems TO dev_goldpoisk;


--
-- TOC entry 3341 (class 0 OID 0)
-- Dependencies: 212
-- Name: product_product_gems_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE product_product_gems_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_product_gems_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE product_product_gems_id_seq TO dev_goldpoisk;


--
-- TOC entry 3343 (class 0 OID 0)
-- Dependencies: 213
-- Name: product_product_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE product_product_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_product_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE product_product_id_seq TO dev_goldpoisk;


--
-- TOC entry 3344 (class 0 OID 0)
-- Dependencies: 214
-- Name: product_product_materials; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE product_product_materials FROM PUBLIC;
REVOKE ALL ON TABLE product_product_materials FROM dev_goldpoisk;
GRANT ALL ON TABLE product_product_materials TO dev_goldpoisk;


--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 215
-- Name: product_product_materials_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE product_product_materials_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_product_materials_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE product_product_materials_id_seq TO dev_goldpoisk;


--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 216
-- Name: product_type; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE product_type FROM PUBLIC;
REVOKE ALL ON TABLE product_type FROM dev_goldpoisk;
GRANT ALL ON TABLE product_type TO dev_goldpoisk;


--
-- TOC entry 3349 (class 0 OID 0)
-- Dependencies: 217
-- Name: product_type_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE product_type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE product_type_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE product_type_id_seq TO dev_goldpoisk;


--
-- TOC entry 3350 (class 0 OID 0)
-- Dependencies: 218
-- Name: shop_admin; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE shop_admin FROM PUBLIC;
REVOKE ALL ON TABLE shop_admin FROM dev_goldpoisk;
GRANT ALL ON TABLE shop_admin TO dev_goldpoisk;


--
-- TOC entry 3352 (class 0 OID 0)
-- Dependencies: 219
-- Name: shop_admin_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE shop_admin_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE shop_admin_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE shop_admin_id_seq TO dev_goldpoisk;


--
-- TOC entry 3354 (class 0 OID 0)
-- Dependencies: 220
-- Name: shop_manager; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE shop_manager FROM PUBLIC;
REVOKE ALL ON TABLE shop_manager FROM dev_goldpoisk;
GRANT ALL ON TABLE shop_manager TO dev_goldpoisk;


--
-- TOC entry 3356 (class 0 OID 0)
-- Dependencies: 221
-- Name: shop_manager_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE shop_manager_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE shop_manager_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE shop_manager_id_seq TO dev_goldpoisk;


--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 222
-- Name: shop_shop; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON TABLE shop_shop FROM PUBLIC;
REVOKE ALL ON TABLE shop_shop FROM dev_goldpoisk;
GRANT ALL ON TABLE shop_shop TO dev_goldpoisk;


--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 223
-- Name: shop_shop_id_seq; Type: ACL; Schema: public; Owner: dev_goldpoisk
--

REVOKE ALL ON SEQUENCE shop_shop_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE shop_shop_id_seq FROM dev_goldpoisk;
GRANT ALL ON SEQUENCE shop_shop_id_seq TO dev_goldpoisk;


-- Completed on 2016-03-03 23:53:26 MSK

--
-- PostgreSQL database dump complete
--

